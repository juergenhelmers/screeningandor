###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class SubstanceFormatValidator < ActiveModel::Validator
  def validate(record)
    unless record.substances.split(",").length == 10
      record.errors[:substances] << " requires exactly 10 comma separated values"
    end
  end
end