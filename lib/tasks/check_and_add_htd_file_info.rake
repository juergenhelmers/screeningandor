###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###


require 'fileutils'
require 'iconv'


namespace :metadata do
  desc "check for HTD file info"
  task :check_htd_file_info => :environment do

  def read_htd_file(raw_htd_file)
    htd_entry = {}                                             # initialize an empty hash for file content
    raw_htd_file.each do |line|                                # read the file object line by line
      ic = Iconv.new('UTF-8//IGNORE', 'UTF-8')                 # replace invalid utf-8 characters (required!)
      line = ic.iconv(line)
      line.chop!                                               # remove any new line characters
      line.gsub!('"', "").sub!(",", "\t")                      # remove extra quotes and replace only the first comma with a tab
      key, value = line.split("\t")                            # split at tab and assign to key and value
      htd_entry[key] = value                                   # save the key value pair in hash
    end
    return htd_entry                                           # return the complete hash
  end

  def get_chanel_names(htd_file_info)
    channel_names = []                              # instantiate a new has to hold channel names
    htd_file_info.each do |key, value|              # loop over the htd_file_info hash
      if key.include? "WaveName"                    # check if the key contains "WaveName"
        channel_names << value.gsub(/^\s|\s+$/, "") # if yes add the new value to the array, remove trailing and leading spaces
      end
    end
    return channel_names
  end


  def process_htd_file(dir,workingplate)
    raw_htd_file = File.open(Dir[dir + "/**/*.{htd,HTD}"][0])          # get the full path to HTD file
    htd_file_info = read_htd_file(raw_htd_file)                        # parse the htd file and save in a hash
    workingplate.htd_file_info = htd_file_info                         # save the htd file info hash in DB
    workingplate.save
    channel_names = get_chanel_names(htd_file_info)                    # get the channel anmes as array  by parsing the htd file info
    htd_file_base_name = File.basename(raw_htd_file).gsub(".htd", "").gsub("HTD", "").gsub(" ", "_") # get the basename of htd file
    channel_name_string = channel_names.join(",")                      # convert array into ,-sep string
    channelFile = File.new("#{dir}/#{htd_file_base_name}channel", "w") # create a new .channel file in same dir as HTD file
    channelFile.write(channel_name_string)                             # write the channel name string into the new file
    channelFile.close                                                  # close the file
  end

  uuid_files =  Dir[APP_CONFIG['remote_file_root_directory'] + "/**/*.uuid"]  # get a list of all uuid files in remote root directory
  uuid_files.each do |uuid_file|                                              # loop over each uuid file
  uuid = File.basename(uuid_file).gsub(".uuid", "")                           # get UUID


  workingplate = Workingplate.find_by_uuid(uuid)

  if workingplate.present?
    if workingplate.imported?
      unless workingplate.htd_file_info.present?  # check for existing htd_file_info
        dir = File.dirname(uuid_file)             # get path to uuid file
        process_htd_file(dir,workingplate)        # process the htd file in directory
      end
    end
  end

  end

  end
end
