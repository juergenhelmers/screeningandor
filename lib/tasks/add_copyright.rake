###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

#Include the HTML class

namespace :copyright do
  desc "Add copyright notice to application files"
  task :add_rb => :environment do

    #Cycles through the views folder and searches for rb files
    Dir["#{Rails.root}/**/*.{rb,rake}"].each do |file_name|

      first_line = open(file_name).gets

      unless first_line == "###\n"
        puts "Adding Copyright To: #{file_name}"
        #Reads erb from file
        content_string = File.open(file_name).read
        copyright_notice = <<-eos
###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

        eos
        file_content = copyright_notice + content_string
        #Writes the new file
        f = File.open(file_name, "w")
        f.write(file_content)
      else
        puts "Already Processed..."
      end

    end

  end

  task :add_haml => :environment do

    #Cycles through the views folder and searches for rb files
    Dir["#{Rails.root}/**/*.haml"].each do |file_name|

      first_line = open(file_name).gets

      unless first_line == "-#\n"
        puts "Adding Copyright To: #{file_name}"
        #Reads erb from file
        content_string = File.open(file_name).read
        copyright_notice = <<-eos
-#
  Copyright (c) 2014 Juergen Helmers - All Rights Reserved
  You may not use, distribute and modify this code under any
  circumstance without the written permission of the author.

  Juergen Helmers <juergen.helmers@gmail.com>


        eos
        file_content = copyright_notice + content_string
        #Writes the new file
        f = File.open(file_name, "w")
        f.write(file_content)
      else
        puts "Already Processed..."
      end

    end

  end
end