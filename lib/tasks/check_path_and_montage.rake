###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###


require 'fileutils'


namespace :images do
  desc "check the image path and montage"
  task :check_path_and_montage => :environment do

  def process_montage(plate,dir)

    ultra_images = Dir[dir + "/**/*1.TIF"]    # get the list of image files in the source directory and sort alphabetically,
    ultra_images << Dir[dir + "/**/*2.TIF"]   # excluding the thumbnails
    image_files = ultra_images.flatten.sort   # linearize the mixed 2D array

    FileUtils.mkdir_p "#{dir}/composites"                          # create new subdirectory in case it does not exist
    image_pairs = image_files.each_slice(2).to_a                   # sort into 2D array group per composite image
    image_pairs.each do |image|                                    # create a composite image combining the two channels
      composite_images = %x[convert -geometry 150x150 '#{image[0]}' '#{image[1]}' -background black -channel green,blue -combine '#{dir}/composites/#{File.basename(image[0]).gsub("_w1","").gsub(".TIF",".png")}']
    end
    # create a montage per well
    FileUtils.mkdir_p "#{dir}/montages"                            # create new subdirectory in case it does not exist
    images = Dir[dir + "/composites/**/*.png"].sort                # read in all composite images
    images_per_well = images.each_slice(4).to_a                    # group per well
    images_per_well.each do |images|
      montage_images = %x[montage -geometry +0+0 -tile 2x2 '#{images[0]}' '#{images[1]}' '#{images[2]}' '#{images[3]}' '#{dir}/montages/#{File.basename(images[0]).gsub("_w1","").gsub("_s1","").gsub(" ", "_")}']
    end

    # save the montage with the workingwell
    montages = Dir[dir + "/montages/**/*.png"].sort                                    # read in all composite images
    workingwells = plate.workingwells.where("name != ? AND name != ?", "PBS", "empty") # get all active workingwells

    workingwells.each_with_index do |well, index|
      well.montage_url = montages[index]
      well.save
    end
  end

  uuid_files =  Dir[APP_CONFIG['remote_file_root_directory'] + "/**/*.uuid"]  # get a list of all uuid files in remote root directory
  uuid_files.each do |uuid_file|                                              # loop over each uuid file
    uuid = File.basename(uuid_file).gsub(".uuid", "")                         # get UUID
    dir = File.dirname(uuid_file)                                             # get path to uuid file

    workingplate = Workingplate.find_by_uuid(uuid)

    if workingplate.present?
      unless workingplate.remote_file_path == dir
        workingplate.remote_file_path = dir
        workingplate.save
        if workingplate.workingwells[13].montage_url == nil
          process_montage(workingplate,dir)
        end
      end
    end

  end

  end
end
