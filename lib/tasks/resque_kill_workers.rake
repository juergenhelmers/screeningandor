###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

namespace :resque do
  desc "Kill running resque workers"
  task :kill_workers => :environment do

    pids = Array.new                                # create an empty array for PIDs
    Resque.workers.each do |worker|                 # loop over workers
      pids << worker.to_s.split(/:/).second         # added the pids to array
    end

    if pids.size > 0
      system("kill -QUIT #{pids.join(' ')}")        # if there are running workers kill them
      Resque.workers.each {|w| w.unregister_worker} # make sure to unregister stale workers as well
    end

  end

end