###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###


require 'fileutils'


namespace :metadata do
  desc "add position numbers to existing metadata_records"
  task :add_position_numbers => :environment do

    positions = {"Catalog Number" => "3", "Mature Sanger ID" => "1", "Precursor Accession" => "4", "Mature Accession" => "2", "Mature Sequence" => "5"}


    Screen.all.each do |screen|
      screen.masterplates.each do |mp|
        mp.masterwells.each do |mw|
          mw.metadata_records.each do |mr|
            mr.update_attributes(position: positions[mr.name])
          end
        end
        mp.workingplates.each do |wp|
          wp.workingwells.each do |ww|
            ww.metadata_records.each do |mr|
              mr.update_attributes(position: positions[mr.name])
            end
          end
        end
      end
    end
  end
end
