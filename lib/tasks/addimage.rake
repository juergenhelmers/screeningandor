###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###


require 'fileutils'


namespace :images do
  desc "move extsing metadata to new metadata_record model"
  task :move_metadata_records => :environment do

    def process_well(well)
      well.metadata_records.create(name: "Catalog Number",      value: "#{well.catalog_number}",      user_id: well.user_id) if well.catalog_number?
      well.metadata_records.create(name: "Mature Sanger ID",    value: "#{well.mature_sanger_id}",    user_id: well.user_id) if well.mature_sanger_id?
      well.metadata_records.create(name: "Precursor Accession", value: "#{well.precursor_accession}", user_id: well.user_id) if well.precursor_accession?
      well.metadata_records.create(name: "Mature Accession",    value: "#{well.mature_accession}",    user_id: well.user_id) if well.mature_accession?
      well.metadata_records.create(name: "Mature Sequence",     value: "#{well.mature_sequence}",     user_id: well.user_id) if well.mature_sequence?
    end





    masterwells = Masterwell.where("catalog_number !=?", "")    # get all master wells with a catalog_number present

    masterwells.each do |masterwell|
      process_well(masterwell)
    end


    workingwells = Workingwell.where("catalog_number !=?", "")  # get all master wells with a catalog_number present
    workingwells.each do |workingwell|
      process_well(workingwell)
    end
    
  end
end
