###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateComparizations < ActiveRecord::Migration
  def change
    create_table :comparizations do |t|
      t.integer :workingwell_id
      t.integer :well_comparison_id

      t.timestamps
    end
  end
end
