###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddIndeces < ActiveRecord::Migration
  def up
    add_index :users, :username
    add_index :screens, :name
    add_index :workingplates, :label
    add_index :workingplates, :masterplate_id
    add_index :masterplates, :screen_id
    add_index :screens, :user_id
    add_index :workingwells, :workingplate_id
    add_index :workingwells, :name
    add_index :workingwells, :uuid
    add_index :workingwells, :mature_sanger_id
    add_index :workingwells, :precursor_accession
    add_index :workingwells, :catalog_number
    add_index :workingwells, :mature_sequence
    add_index :analytical_results, :name
  end

  def down
    remove_index :users, :username
    remove_index :screens, :name
    remove_index :workingplates, :label
    remove_index :workingwells, :name
    remove_index :workingwells, :uuid
    remove_index :workingwells, :mature_sanger_id
    remove_index :workingwells, :precursor_accession
    remove_index :workingwells, :catalog_number
    remove_index :workingwells, :mature_sequence
    remove_index :analytical_results, :name
  end
end
