###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddOrganismIdToScreen < ActiveRecord::Migration
  def change
    add_column :screens, :organism_id, :integer
  end
end
