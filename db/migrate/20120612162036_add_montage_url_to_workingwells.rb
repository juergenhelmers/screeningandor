###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddMontageUrlToWorkingwells < ActiveRecord::Migration
  def change
    add_column :workingwells, :montage_url, :string

  end
end
