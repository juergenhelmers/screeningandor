###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddHtdFileIntoToWorkingplates < ActiveRecord::Migration
  def change
    add_column :workingplates, :htd_file_info, :text
  end
end
