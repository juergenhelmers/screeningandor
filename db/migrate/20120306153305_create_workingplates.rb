###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateWorkingplates < ActiveRecord::Migration
  def change
    create_table :workingplates do |t|
      t.string :uuid
      t.string :label
      t.integer :masterplate_id
      t.text :description
      t.integer :user_id

      t.timestamps
    end
  end
end
