###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateAnalyticalResults < ActiveRecord::Migration
  def change
    create_table :analytical_results do |t|
      t.string :name
      t.string :value
      t.integer :workingwell_id

      t.timestamps
    end
  end
end
