###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateMetadataRecords < ActiveRecord::Migration
  def change
    create_table :metadata_records do |t|
      t.string :name
      t.string :value
      t.string :uuid
      t.boolean :database
      t.string :url

      t.timestamps
    end
  end
end
