###

class CreateScreens < ActiveRecord::Migration
  def change
    create_table :screens do |t|
      t.string :name
      t.string :uuid
      t.integer :user_id

      t.timestamps
    end
  end
end
