###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateAnalyticResults < ActiveRecord::Migration
  def change
    create_table :analytic_results do |t|
      t.integer :screen_id
      t.string :name

      t.timestamps
    end
  end
end
