###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateWellComparisons < ActiveRecord::Migration
  def change
    create_table :well_comparisons do |t|
      t.text :description
      t.string :uuid
      t.integer :user_id

      t.timestamps
    end
  end
end
