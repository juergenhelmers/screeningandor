###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateScreenSettings < ActiveRecord::Migration
  def change
    create_table :screen_settings do |t|
      t.integer :screen_id
      t.text :substances
      t.integer :csv_first_data_row

      t.timestamps
    end
  end
end
