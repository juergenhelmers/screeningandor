###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddDescriptionToScreen < ActiveRecord::Migration
  def change
    add_column :screens, :description, :text

  end
end
