###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AddImportedToWorkingplates < ActiveRecord::Migration
  def change
    add_column :workingplates, :imported, :boolean, :default => false, :null => false

    add_column :workingplates, :remote_file_path, :string

  end
end
