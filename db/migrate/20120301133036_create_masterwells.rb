###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class CreateMasterwells < ActiveRecord::Migration
  def change
    create_table :masterwells do |t|
      t.integer :masterplate_id
      t.string :name
      t.string :uuid
      t.string :catalog_number
      t.string :mature_sanger_id
      t.string :precursor_accession
      t.string :mature_accession
      t.string :mature_sequence
      t.integer :user_id

      t.timestamps
    end
  end
end
