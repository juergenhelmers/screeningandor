###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(email: "test@example.com", firstname: "Klaus", lastname: "Meyer", username: "klaus", password: "IOP999jkl", password_confirmation: "IOP999jkl")
User.create(email: "juergen.helmers@gmail.com", firstname: "Juergen", lastname: "Helmers", username: "helmerj", password: "#{ENV['DB_PASSWORD']}", password_confirmation: "#{ENV['DB_PASSWORD']}", approved: true, admin: true, receives_system_email: true)

Organism.create(name: "homo sapiens", description: "human")
Organism.create(name: "mus musculus", description: "mouse")
Organism.create(name: "rattus norvegicus", description: "rat")
