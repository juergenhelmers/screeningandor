# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140410092658) do

  create_table "analytic_results", :force => true do |t|
    t.integer  "screen_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "analytical_results", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.integer  "workingwell_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "heatmap",        :default => false
  end

  add_index "analytical_results", ["name"], :name => "index_analytical_results_on_name"

  create_table "comparizations", :force => true do |t|
    t.integer  "workingwell_id"
    t.integer  "well_comparison_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "masterplates", :force => true do |t|
    t.string   "uuid"
    t.string   "label"
    t.integer  "screen_id"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "masterplates", ["screen_id"], :name => "index_masterplates_on_screen_id"

  create_table "masterwells", :force => true do |t|
    t.integer  "masterplate_id"
    t.string   "name"
    t.string   "uuid"
    t.string   "catalog_number"
    t.string   "mature_sanger_id"
    t.string   "precursor_accession"
    t.string   "mature_accession"
    t.string   "mature_sequence"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "metadata_records", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.string   "uuid"
    t.boolean  "database"
    t.string   "url"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "user_id"
    t.integer  "metadatarecordable_id"
    t.string   "metadatarecordable_type"
    t.integer  "position"
  end

  create_table "organisms", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "screen_settings", :force => true do |t|
    t.integer  "screen_id"
    t.text     "substances"
    t.integer  "csv_first_data_row"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "screens", :force => true do |t|
    t.string   "name"
    t.string   "uuid"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "description"
    t.integer  "organism_id"
  end

  add_index "screens", ["name"], :name => "index_screens_on_name"
  add_index "screens", ["user_id"], :name => "index_screens_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "firstname",              :default => "",    :null => false
    t.string   "lastname",               :default => "",    :null => false
    t.string   "username",               :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false, :null => false
    t.boolean  "approved",               :default => false, :null => false
    t.boolean  "receives_system_email",  :default => false
  end

  add_index "users", ["approved"], :name => "index_users_on_approved"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username"

  create_table "well_comparisons", :force => true do |t|
    t.text     "description"
    t.string   "uuid"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "workingplates", :force => true do |t|
    t.string   "uuid"
    t.string   "label"
    t.integer  "masterplate_id"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "imported",         :default => false, :null => false
    t.string   "remote_file_path"
    t.boolean  "processing",       :default => false
    t.text     "htd_file_info"
  end

  add_index "workingplates", ["label"], :name => "index_workingplates_on_label"
  add_index "workingplates", ["masterplate_id"], :name => "index_workingplates_on_masterplate_id"

  create_table "workingwells", :force => true do |t|
    t.integer  "workingplate_id"
    t.string   "name"
    t.string   "uuid"
    t.string   "catalog_number"
    t.string   "mature_sanger_id"
    t.string   "precursor_accession"
    t.string   "mature_accession"
    t.string   "mature_sequence"
    t.boolean  "is_control"
    t.string   "substance"
    t.string   "concentration"
    t.string   "buffer"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "montage_url"
  end

  add_index "workingwells", ["catalog_number"], :name => "index_workingwells_on_catalog_number"
  add_index "workingwells", ["mature_sanger_id"], :name => "index_workingwells_on_mature_sanger_id"
  add_index "workingwells", ["mature_sequence"], :name => "index_workingwells_on_mature_sequence"
  add_index "workingwells", ["name"], :name => "index_workingwells_on_name"
  add_index "workingwells", ["precursor_accession"], :name => "index_workingwells_on_precursor_accession"
  add_index "workingwells", ["uuid"], :name => "index_workingwells_on_uuid"
  add_index "workingwells", ["workingplate_id"], :name => "index_workingwells_on_workingplate_id"

end
