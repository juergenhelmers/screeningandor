###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###
#RVM setup
require "rvm/capistrano"                                            # Load RVM's capistrano plugin.
require "bundler/capistrano"
set :rvm_type, :user
set :rvm_ruby_string, 'ruby-1.9.3-p484@screeningandor'              # use 1.9.3@gemsetname for using RVM gemset


# server details
set :application, "screeningandor"                                  # set the application name also used as subdirectory
set :deploy_to, "/home/rubyonrails/rails/capistrano/#{application}" # set the target directory

default_run_options[:pty] = true
ssh_options[:forward_agent] = true                                  # use ssh forwarding to allow login w/o password on kentridge
set :scm, :git                                                      # specify git as repository
set :repository, "ssh://git@bitbucket.org/juergenhelmers/screeningandor.git" # set the repository on bitbucket
set :branch, "master"                                               # specify the branch to check out
set :scm_command, "/usr/bin/git"                                    # specify the full path to git


set :user, "rubyonrails"                                            # indicate the remote user to be kentridge
set :use_sudo, false                                                # do not use sudo
set :lorax, "146.64.120.74"                                         # create variable lorax, set to IP
set :keep_releases, 5
role :web, lorax                                                    # set web server
role :app, lorax                                                    # set application server
role :db,  lorax, :primary => true                                  # set database server
set :gateway, 'juergen@mhlangalab.synbio.csir.co.za'                # specify the web-server as a gateway


# hooks
after 'deploy:update_code', 'bundle_gems'                           # update gems after code update
after "deploy:update_code", "deploy:create_config_files"            # copy the database config file
after "deploy:update_code", "deploy:migrate"                        # migrate the database
after "deploy:create_symlink", 'restart_resque_workers'             # restart the resque workers
after "deploy:update_code", "deploy:pipeline_precompile"
after "deploy:update", "deploy:cleanup"
before "deploy:restart", 'clear_sessions'                           # clear all session before restarting the server


namespace :deploy do

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  task :create_config_files, :roles => :app do                      # create the required configuration files
    run "ln -fs #{File.join(release_path,'config','database.yml.deploy')} #{File.join(release_path,'config','database.yml')}"
    run "ln -fs #{File.join(release_path,'config','config.yml.deploy')} #{File.join(release_path,'config','config.yml')}"
    run "ln -fs #{File.join(release_path,'config','resque-production.god.deploy.rb')} #{File.join(release_path,'config','resque-production.god')}"
    end

  task :pipeline_precompile do
    run "cd #{release_path}; RAILS_ENV=production bundle exec rake assets:precompile"
  end
end
 
task :bundle_gems, :roles => :app do                                 # install the required gems using bundle
  run "mkdir -p #{shared_path}/bundle && ln -s #{shared_path}/bundle #{release_path}/vendor/bundle"
  run "cd #{latest_release}; RAILS_ENV=production bundle exec bundle install --path vendor/bundle --gemfile Gemfile --deployment --without development test"
end

desc "Restart the resque workers on remote server"
task :restart_resque_workers do
  run("cd #{deploy_to}/current; /usr/bin/env RAILS_ENV=production rake resque:kill_workers")
  # not required anymore as monitored by the god gem
  #run("cd #{deploy_to}/current; /usr/bin/env RAILS_ENV=production TERM_CHILD=1 BACKGROUND=yes COUNT=8 QUEUE='ome_rewrite' RESQUE_WORKER=true rake resque:workers")
end

task :kill_resque_workers do
  run("cd #{deploy_to}/current; /usr/bin/env RAILS_ENV=production rake resque:kill_workers")
end

task :clear_sessions do
  run( "cd #{latest_release}; RAILS_ENV=production bundle exec rake tmp:sessions:clear")
end


desc "List used application server libraries"
task :list_gems, :roles => :app do
  run "gem list"
end
