###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
Screeningandor::Application.config.secret_token = 'e42eb45b20ed48e8ec5322564a606fa97052a3a776f4ee428da984ae38266ed6ed7c6df5c6e1233ccaf81129c95fd8fadc66771d9d66f0250ff42e6615ec8cf1'
