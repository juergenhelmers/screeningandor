###
# Copyright (C) 2013 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

rails_env   = ENV['RAILS_ENV']  || "production"
num_workers = rails_env == 'production' ? 8 : 2

num_workers.times do |num|
  God.watch do |w|
    w.name     = "resque-#{num}"
    w.interval = 30.seconds
    w.env = { 'RAILS_ENV' => rails_env, 'QUEUE' => 'ome_rewrite', 'RESQUE_WORKER' => 'true', 'TERM_CHILD' => '1' }
    w.uid = 'rubyonrails'
    w.gid = 'rubyonrails'
    w.dir = File.expand_path(File.join(File.dirname(__FILE__),'..'))
    w.start = "bundle exec rake resque:work"
    w.start_grace = 10.seconds
    #w.log = File.expand_path(File.join(File.dirname(__FILE__), "..","log","resque-worker-#{num}.log"))

    # restart if memory gets too high
    w.transition(:up, :restart) do |on|
      on.condition(:memory_usage) do |c|
        c.above = 400.megabytes
        c.times = 2
      end
    end

    # determine the state on startup
    w.transition(:init, { true => :up, false => :start }) do |on|
      on.condition(:process_running) do |c|
        c.running = true
      end
    end

    # determine when process has finished starting
    w.transition([:start, :restart], :up) do |on|
      on.condition(:process_running) do |c|
        c.running = true
        c.interval = 5.seconds
      end

      # failsafe
      on.condition(:tries) do |c|
        c.times = 5
        c.transition = :start
        c.interval = 5.seconds
      end
    end

    # start if process is not running
    w.transition(:up, :start) do |on|
      on.condition(:process_running) do |c|
        c.running = false
      end
    end
  end
end
