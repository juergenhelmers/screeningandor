###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

Screeningandor::Application.routes.draw do

  resources :well_comparisons do
    member do
      get 'compare_with_control'
    end
  end

  get "send_images/show_file_by_send"

  resources :organisms

  resources :analytic_results

  resources :screen_settings

  devise_for :users

  resources :users, :except => [:new, :show] do
    member do
      get 'approve'
      get 'disable'
      get 'grant_admin'
      get 'deny_admin'
      get 'grant_receive_system_mail'
      get 'deny_receive_system_mail'
    end
  end

  resources :welcome, :only => :index
  
  resources :workingwells do
    member do
      get 'add_to_compare'
      get 'remove_from_compare'
    end
    collection do
      get 'clear_comparison_cart'
    end
  end

  match 'searches', to: 'workingwells#index', via: :get

  match 'jqueryfiletree/:id/content' => 'jqueryfiletree#content'#, :as => :content
  resources :jqueryfiletree do
    collection do
      post 'content'
    end
  end

  resources :workingplates do
    member do
      get 'omeroimport'
      post 'import'
      get 'generate_uuid_file'
    end
  end

  resources :masterwells, :only => [:new, :create, :show ]

  resources :masterplates

  resources :screens

  resources :well_names
  resources :well_uuids
  resources :metadata_record_values
  #resources :well_mature_sanger_ids
  #resources :well_precursor_accessions
  #resources :well_catalog_numbers
  #resources :well_mature_sequences


  authenticate :user do
    mount Resque::Server.new, :at => "/resque", :as => 'resque'
  end

  root :to => 'welcome#index'

end
