###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Screeningandor::Application.initialize!
