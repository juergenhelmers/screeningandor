###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe AnalyticalResult do
  it "has a valid fixture" do
    expect(FactoryGirl.create(:analytical_result)).to be_valid
  end

  it "has a valid heatmap fixture" do
    expect(FactoryGirl.create(:analytical_result_hm)).to be_valid
  end

  it "is not valid without a name" do
    expect(FactoryGirl.build(:analytical_result, name: nil)).to_not be_valid
  end

  it "is not valid without a value" do
    expect(FactoryGirl.build(:analytical_result, value: nil)).to_not be_valid
  end

  it "returns heatmap reuslts using scope" do
    ar = FactoryGirl.create(:analytical_result_hm)
    expect(AnalyticalResult.heatmap_data.count).to eq(1)
  end

end
