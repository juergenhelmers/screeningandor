###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe Organism do
  it "has a valid fixture" do
    expect(FactoryGirl.create(:organism)).to be_valid
  end

  it "is not valid without a name" do
    expect(FactoryGirl.build(:organism, name: nil)).to_not be_valid
  end

  it "is not valid with a duplicate name" do
    org1 = FactoryGirl.create(:organism, name: "RAT")
    expect(FactoryGirl.build(:organism, name: org1.name)).to_not be_valid
  end
end
