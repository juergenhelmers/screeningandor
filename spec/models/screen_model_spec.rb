###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe Screen do
  it "has a valid factory" do
    expect(FactoryGirl.create(:screen)).to be_valid
  end
  it "is invalid without a name" do
    expect(FactoryGirl.build(:screen, name: nil)).to_not be_valid
  end
  it "is invalid without a unique name" do
    Screen.destroy_all
    screen = FactoryGirl.create(:screen, name: "test screen")
    expect(FactoryGirl.build(:screen, name: screen.name)).to_not be_valid
  end
  it "it should create a UUID" do
    screen = FactoryGirl.create(:screen)
    expect(screen.uuid).to be_present
  end
end
