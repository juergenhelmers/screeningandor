###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe MetadataRecord do

  describe "Masterwell related records" do
    it "should have a valid factory" do
      expect(FactoryGirl.create(:metadata_record)).to be_valid
    end

    it "should only be valid with a name" do
      expect(FactoryGirl.build(:metadata_record, name: nil)).to_not be_valid
    end

    it "should only be valid with a value" do
      expect(FactoryGirl.build(:metadata_record, value: nil)).to_not be_valid
    end

    it "should only be valid with a position" do
      expect(FactoryGirl.build(:metadata_record, position: nil)).to_not be_valid
    end

    it "should not be valid with an url if it is a database record" do
      expect(FactoryGirl.build(:metadata_record_db, url: nil)).to_not be_valid
    end
  end

  describe "workingwell related records" do
    it "should have a valid factory" do
      expect(FactoryGirl.create(:metadata_record_workingwell)).to be_valid
    end

    it "should only be valid with a name" do
      expect(FactoryGirl.build(:metadata_record_workingwell, name: nil)).to_not be_valid
    end

    it "should only be valid with a value" do
      expect(FactoryGirl.build(:metadata_record_workingwell, value: nil)).to_not be_valid
    end

  end

end