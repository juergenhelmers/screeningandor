###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require "spec_helper"

describe Workingwell do

  it "should have a valid factory" do
    expect(FactoryGirl.create(:workingwell)).to be_valid
  end

  it "should require a name" do
    expect(FactoryGirl.build(:workingwell, name: nil)).to_not be_valid
  end

  it "should require a uuid" do
    expect(FactoryGirl.build(:workingwell, uuid: nil)).to_not be_valid
  end
  end