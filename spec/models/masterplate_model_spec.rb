###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe Masterplate do

  it "has a valid factory" do
    expect(FactoryGirl.create(:masterplate)).to be_valid
  end

  it "is invalid without a label" do
    expect(FactoryGirl.build(:masterplate, label: nil)).to_not be_valid
  end

  it "is invalid without a masterplate_annotation_file" do
    expect(FactoryGirl.build(:masterplate, masterplate_annotation_file: nil)).to_not be_valid
  end

  it "it should create a UUID" do
    masterplate = FactoryGirl.create(:masterplate)
    expect(masterplate.uuid).to be_present
  end

end