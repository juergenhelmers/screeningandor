###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe Workingplate do
  it "has a valid factory" do
    expect(FactoryGirl.build(:workingplate)).to be_valid
  end

  it "should create a UUID" do
    workingplate = FactoryGirl.create(:workingplate)
    expect(workingplate.uuid).to be_present
  end

  it "should parse an htd file and return a hash" do
    htd_file = "#{Rails.root}/db/misc/test.HTD"
    htd_hash = Workingplate.read_htd_file(htd_file)
    expect(htd_hash['WaveName1']).to eq(" FITC")
    expect(htd_hash['WaveName2']).to eq(" DAPI")
  end

  it "should return channel names when requesting them" do
    htd_file = "#{Rails.root}/db/misc/test.HTD"
    htd_hash = Workingplate.read_htd_file(htd_file)
    channel_names = Workingplate.get_channel_names(htd_hash)
    expect(channel_names.length).to eq(2)
    expect(channel_names['WaveName1']).to eq("FITC")
    expect(channel_names['WaveName2']).to eq("DAPI")
  end

  # it "should return image_type when requested" do
  #   tif_file = "#{Rails.root}/db/misc/test.TIF"
  #   expect(Workingplate.image_type(tif_file)).to eq("MetaMorph")
  # end

  it "should return unknown if neither Andor nor MetaMorph file" do
    htd_file = "#{Rails.root}/db/misc/test.HTD"
    file_type = Workingplate.image_type(htd_file)
    expect(file_type).to eq("unknown")
  end
end
