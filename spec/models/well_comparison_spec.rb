###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe WellComparison do
  it "has a valid fixture" do
    expect(FactoryGirl.create(:well_comparison)).to be_valid
  end

  it "is not valid without a user_id" do
    expect(FactoryGirl.build(:well_comparison, user_id: nil)).to_not be_valid
  end

  it "it should create a UUID" do
    wc = FactoryGirl.create(:well_comparison)
    expect(wc.uuid).to be_present
  end

end
