###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "MetadataRecords" do
  before(:each) do
    user = FactoryGirl.create(:user)
    visit('/users/sign_in')

    fill_in('Username', :with => user.username)
    fill_in('Password', :with => user.password)
    #save_and_open_page
    click_button('Sign in')
  end

  it "should display the metadata key value pair" do
    metadata_record = FactoryGirl.create(:metadata_record)
    visit(masterwell_path(metadata_record.metadatarecordable))
    expect(page).to have_content(metadata_record.name)
    expect(page).to have_content(metadata_record.value)
  end

  it "should display the metadata key value pair for a workingwell" do
    metadata_record = FactoryGirl.create(:metadata_record_workingwell, position: 0)
    visit(workingwell_path(metadata_record.metadatarecordable))
    expect(page).to have_content(metadata_record.name)
    expect(page).to have_content(metadata_record.value)
  end

  it "should list working well when searching for metadata" do
    metadata_record = FactoryGirl.create(:metadata_record_workingwell,  position: 0)
    visit(workingwells_path)
    fill_in('Metadata', with: metadata_record.value)
    click_button('Search')
    within('table#search.table.table-striped') do
      expect(page).to have_content(metadata_record.value)
      expect(page).to have_content(metadata_record.name)
      expect(page).to have_content(metadata_record.metadatarecordable.workingplate.label)
    end

  end

end