###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "Workingwells" do

  before(:each) do
    user = FactoryGirl.create(:user)
    visit('/users/sign_in')

    fill_in('Username', :with => user.username)
    fill_in('Password', :with => user.password)
    #save_and_open_page
    click_button('Sign in')
  end


  it "does return code 200 when accessing the show page for a well" do
    wwell=FactoryGirl.create(:workingwell)
    visit(workingwell_path(wwell))
    expect(page.status_code).to be(200)
  end

  it "does list the plate, metadata and euclidiean distance of a well" do
    AnalyticalResult.destroy_all
    ar = FactoryGirl.create(:analytical_result_hm)
    well = ar.workingwell
    visit(searches_path)
    expect(page).to have_content(well.analytical_results.first.value)
    expect(page).to have_content(well.workingplate.label)
  end

  describe "clicking the compare button" do
    before(:each) do
      ar = FactoryGirl.create(:analytical_result)
      @well = ar.workingwell
      visit(searches_path)
      within("tr.well_#{@well.id}") do
        click_link('Compare')
      end
    end

    it "clicking the compare button changes its class to btn-danger" do
      expect(page).to have_css("tr.well_#{@well.id} a.btn-danger")
    end

    it "a compare click adds a new row to the compare table in the side bar" do
      expect(page).to have_css("tr.row_#{@well.id} a.btn-danger")
      within('#user_nav.well_comparison')do
        expect(page).to have_content(@well.name)
      end
    end
  end

  # describe "use AJAX calls to test comparison cart feature" do
  #   before(:each) do
  #     ar = FactoryGirl.create(:analytical_result)
  #     @well = ar.workingwell
  #     visit(searches_path)
  #     within("tr.well_#{@well.id}") do
  #       click_link('Compare')
  #     end
  #   end
  #
  #   it "changes the class of the compare button upon clicking it", js: true do
  #     expect(page).to have_css("tr.well_#{@well.id} a.btn-danger")
  #   end
  # end

end
