###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "Users" do

  describe "admin user" do
    before(:each) do
      admin = FactoryGirl.create(:admin)
      visit('/users/sign_in')

      fill_in('Username', :with => admin.username)
      fill_in('Password', :with => admin.password)
      click_button('Sign in')
    end

    it "can see the users and organism menu links" do
      visit(root_path)
      within('ul.nav') do
        expect(page).to have_content('Users')
        expect(page).to have_content('Organisms')
      end
    end
    it "admin can see the edit and destroy buttons" do
      user = FactoryGirl.create(:user)
      visit(users_path)
      expect(page).to have_content('Edit')
      expect(page).to have_content('Destroy')
    end

    it "admin can see the user administration links on the update page" do
      user = FactoryGirl.create(:user)
      visit(edit_user_path(user))
      expect(page).to have_content('disable User')
      expect(page).to have_content('grant admin privileges')
      expect(page).to_not have_content('send user system emails')
    end

    it "admin can make user admin" do
      user = FactoryGirl.create(:user)
      visit(edit_user_path(user))
      expect(page).to have_content('disable User')
      expect(page).to have_content('grant admin privileges')
      expect(page).to_not have_content('send user system emails')
      click_link('grant admin privileges')
      visit(edit_user_path(user))
      expect(page).to have_content('disable User')
      expect(page).to have_content('deny admin privileges')
      expect(page).to have_content('send user system emails')
    end

    it "admin can make user admin" do
      user = FactoryGirl.create(:admin)
      visit(edit_user_path(user))
      expect(page).to have_content('disable User')
      expect(page).to have_content('deny admin privileges')
      expect(page).to have_content('send user system emails')
      click_link('send user system emails')
      visit(edit_user_path(user))
      expect(page). to have_content('do no longer send user system emails')
    end

  end


  describe "non admin users" do
    before(:each) do
      user = FactoryGirl.create(:user)
      visit('/users/sign_in')

      fill_in('Username', :with => user.username)
      fill_in('Password', :with => user.password)
      click_button('Sign in')
    end

    it "non admin cannot see the edit and destroy buttons" do
      user = FactoryGirl.create(:user)
      visit(users_path)
      expect(page).to_not have_content('Edit')
      expect(page).to_not have_content('Destroy')
    end
  end
end