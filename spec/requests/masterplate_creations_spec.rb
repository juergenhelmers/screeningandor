###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "MasterplateCreations" do
  def create_screen
    org = FactoryGirl.create(:organism)
    visit(new_screen_path)
    fill_in :screen_name, with: "HIV test Screen"
    fill_in :screen_description, with: "HIV test Screen Description"
    choose org.name
    click_button('Create Screen')
  end

  before(:each) do
    @user = FactoryGirl.create(:user)
    visit('/users/sign_in')
    fill_in('Username', :with => @user.username)
    fill_in('Password', :with => @user.password)
    click_button('Sign in')
  end

  it "should create a masterplate with valid attributes from Excelx file" do
    create_screen
    visit(new_masterplate_path)
    fill_in("Label Root", :with => 'HIV Test')
    select('HIV test Screen', :from => 'Screen')
    fill_in('Sequence Number Offset', with: '1')
    attach_file('Annotation File', "#{Rails.root}/spec/fixtures/files/masterplate.xlsx")
    click_button('Create Masterplate')
    expect(current_path).to eq(masterplates_path)
    expect(page).to have_content("Masterplates successfully created.")
  end

  it "should create a masterplate with valid attributes from Excel file" do
    create_screen
    visit(new_masterplate_path)
    fill_in("Label Root", :with => 'HIV Test')
    select('HIV test Screen', :from => 'Screen')
    fill_in('Sequence Number Offset', with: '1')
    attach_file('Annotation File', "#{Rails.root}/spec/fixtures/files/masterplate.xls")
    click_button('Create Masterplate')
    expect(current_path).to eq(masterplates_path)
    expect(page).to have_content("Masterplates successfully created.")
    expect(page).to have_content("HIV Test")
  end

  it "has the correct wells and metadata_records attached from Excel file" do
    create_screen
    visit(new_masterplate_path)
    fill_in("Label Root", :with => 'HIV Test')
    select('HIV test Screen', :from => 'Screen')
    fill_in('Sequence Number Offset', with: '1')
    attach_file('Annotation File', "#{Rails.root}/spec/fixtures/files/masterplate.xls")
    click_button('Create Masterplate')
    visit(masterplates_path)
    click_link("HIV Test-01")
    click_link("A02")
    expect(page).to have_content("C-300879-01")
  end

  it "has the correct wells and metadata_records attached from Excelx file" do
    create_screen
    visit(new_masterplate_path)
    fill_in("Label Root", :with => 'HIV Test')
    select('HIV test Screen', :from => 'Screen')
    fill_in('Sequence Number Offset', with: '1')
    attach_file('Annotation File', "#{Rails.root}/spec/fixtures/files/masterplate.xlsx")
    click_button('Create Masterplate')
    visit(masterplates_path)
    click_link("HIV Test-01")
    click_link("A02")
    expect(page).to have_content("C-300879-01")
  end

  it "should not create a masterplate with without a label root" do
    visit(new_masterplate_path)
    expect(current_path).to eq(new_screen_path)
    expect(page).to have_content("You need to create a screen before creating masterplates")
  end

  it "should belong to current user" do
    create_screen
    visit(new_masterplate_path)
    fill_in 'Label Root', with: "HIV test"
    select('HIV test Screen', :from => 'Screen')
    attach_file('Annotation File', "#{Rails.root}/spec/fixtures/files/masterplate.xlsx")
    fill_in('Sequence Number Offset', with: '1')
    click_button('Create Masterplate')
    expect(current_path).to eq(masterplates_path)
    expect(page).to have_content("HIV test")
  end
end
