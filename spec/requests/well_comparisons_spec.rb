###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "WellComparisons" do
  describe "not logged in" do
    it "should redirect to sign in page" do
      visit well_comparisons_path
      expect(page).to have_content("You need to sign in or sign up before continuing.")
    end
  end

  describe "authenticated user" do
    before(:each) do
      user = FactoryGirl.create(:user)
      visit('/users/sign_in')
      fill_in('Username', :with => user.username)
      fill_in('Password', :with => user.password)
      click_button('Sign in')
    end


    it "does return code 200 when accessing the index page" do
      visit(well_comparisons_path)
      expect(page.status_code).to be(200)
    end

    # it "should create a well_comparison with valid attributes" do
    #   ar = FactoryGirl.create(:analytical_result_hm)
    #   session[:compare] << "'#{ar.workingwell.id}' => '#{ar.workingwell.id}'"
    #   visit new_well_comparison_path
    #   fill_in('Description', with: 'Lorem Ipsum, alia iacta est.')
    #   click_button 'Save Comparison'
    #   expect(page).to have_content("Well Comparison was successfully saved.")
    #   expect(page).to have_content('Lorem Ipsum, alia iacta est.')
    # end



  end
end