###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "AnalyticalResults" do

  before(:each) do
    user = FactoryGirl.create(:user)
    visit('/users/sign_in')

    fill_in('Username', :with => user.username)
    fill_in('Password', :with => user.password)
    #save_and_open_page
    click_button('Sign in')
  end


  it "does return code 200 when accessing the show page for a workingwell with an analytical result associated" do
    ar=FactoryGirl.create(:analytical_result)
    visit(workingwell_path(ar.workingwell))
    expect(page.status_code).to be(200)
  end

end
