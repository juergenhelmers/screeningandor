###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "ScreenCreations" do

  before(:each) do
    user = FactoryGirl.create(:user)
    visit('/users/sign_in')

    fill_in('Username', :with => user.username)
    fill_in('Password', :with => user.password)
    #save_and_open_page
    click_button('Sign in')
  end

  it "does return code 200 when accessing the index page" do
    visit(screens_path)
    expect(page.status_code).to be(200)
  end

  it "should create a screen with valid attributes" do
    organism = FactoryGirl.create(:organism)
    visit(new_screen_path)
    fill_in :screen_name, with: "HIV test Screen"
    fill_in :screen_description, with: "HIV test Screen Description"
    choose organism.name
    click_button('Create Screen')
    expect(page).to have_content("Screen was successfully created")
  end

  it "should NOT create a screen without all valid attributes" do
    visit(new_screen_path)
    click_button('Create Screen')
    expect(page).to have_content("Name can't be blank")
  end



end
