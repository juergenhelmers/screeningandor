###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'spec_helper'

describe "Organisms" do


  describe "non admin user" do
    before(:each) do
      user = FactoryGirl.create(:user)
      visit('/users/sign_in')

      fill_in('Username', :with => user.username)
      fill_in('Password', :with => user.password)
      #save_and_open_page
      click_button('Sign in')
    end

    it "does return code 200 when accessing the index page" do
      visit(organisms_path)
      expect(page.status_code).to be(200)
    end

    it "index page should not have any admin links" do
      FactoryGirl.create(:organism)
      visit(organisms_path)
      expect(page).to_not have_content("New Organism")
      expect(page).to_not have_content("Edit")
      expect(page).to_not have_content("Destroy")
    end
  end


  describe "admin user" do
    before(:each) do
      user = FactoryGirl.create(:admin)
      visit('/users/sign_in')

      fill_in('Username', :with => user.username)
      fill_in('Password', :with => user.password)
      #save_and_open_page
      click_button('Sign in')
    end

    it "index page should have admin links" do
      organism = FactoryGirl.create(:organism)
      visit(organisms_path)
      expect(page).to have_content("New Organism")
      expect(page).to have_content("Edit")
      expect(page).to have_content("Destroy")
    end
    
    it "should create an organism with valid attributes" do
      visit(new_organism_path)
      fill_in 'Name', with: "Homo Sapiens"
      fill_in 'Description', with: "Test Organism"

      click_button('Create Organism')
      expect(page).to have_content("Organism was successfully created")
    end

    it "should fail to create a new Organism without valid attributes" do
      visit(new_organism_path)
      fill_in 'Description', with: "Test Organism"

      click_button('Create Organism')
      expect(page).to have_content("Name can't be blank")
    end

    it "can delete an organism" do
      organism = FactoryGirl.create(:organism)
      visit(organisms_path)
      expect{
        within("tr#organism#{organism.id}") do
          click_link('Destroy')
        end
      }.to change(Organism,:count).by(-1)
      expect(page).to have_content('Organism was successfully deleted.')
      expect(page).to_not have_content(organism.name)
    end
  end


end
