require 'spec_helper'

describe WorkingplateBuilder do
  include Rack::Test::Methods
  include ActionDispatch::TestProcess

  let(:user){FactoryGirl.create(:user)}
  let(:screen){FactoryGirl.create(:screen, user: user)}
  let!(:screen_setting){ FactoryGirl.create(:screen_setting, screen: screen) }
  # move the masterplate creation into a fixture for faster test execution
  let(:params_mp){
      {masterplate: {
        label: 'test',
        masterplate_sequence_number_offset: 1,
        screen_id: screen.id,
       }
     }
  }
  let(:params){
    {workingplate: {
        description: "test description",
        mp_id: Masterplate.first.id,
      }
    }
  }

  subject { described_class.new(params, user.id) }
  before :each do
    @file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/masterplate.xlsx'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    params_mp[:masterplate].merge!(masterplate_annotation_file: @file)
    MasterplateBuilder.new(params_mp, user.id).build
  end

  it "creates a workingplate based on the parent masterplate" do
    masterplate = Masterplate.first
    subject.build
    expect(Workingplate.all.count).to eq(1)
    expect(Workingplate.first.workingwells.count).to eq(96)
    expect(Workingplate.first.user).to eq(user)
    expect(Workingplate.first.masterplate).to eq(masterplate)
  end

  it "creates a workingplate of a particular layout" do
    subject.build
    expect(Workingplate.first.workingwells[0..11].map{|w| w.name }.uniq).to eq(['PBS'])
    expect(Workingplate.first.workingwells[25].name).to eq('A02')
    expect(Workingplate.first.workingwells[13].is_control?).to eq(true)
    expect(Workingplate.first.workingwells[41].name).to eq('empty')
    expect(Workingplate.first.workingwells.where(name: 'empty').count).to eq(26)
  end

end
