require 'spec_helper'

describe MasterplateBuilder do
  include Rack::Test::Methods
  include ActionDispatch::TestProcess
  let(:user){FactoryGirl.create(:user)}
  let(:screen){FactoryGirl.create(:screen, user: user)}
  let(:params){
      {masterplate: {
        label: 'test',
        masterplate_sequence_number_offset: 1,
        screen_id: screen.id,
       }
     }
  }
  subject { described_class.new(params, user.id) }
  before :each do
    @file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/masterplate.xlsx'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    params[:masterplate].merge!(masterplate_annotation_file: @file)
  end



  it "creates a masterplate with masterwells based on the input excel file" do
    subject.build
    expect(Masterplate.all.count).to eq(1)
    expect(Masterplate.first.masterwells.count).to eq(17)
    expect(Masterplate.first.user).to eq(user)
  end

  it "creates masterwell records based on the input file fields" do
    subject.build
    expect(Masterplate.first.masterwells.second.name).to eq('A02')
    expect(Masterplate.first.masterwells.second.metadata_records.count).to eq(6)
    expect(Masterplate.first.masterwells.second.metadata_records.first.name).to eq('Order Number')
    expect(Masterplate.first.masterwells.second.metadata_records.first.value).to eq('228882.0')
    expect(Masterplate.first.masterwells.second.metadata_records.first.metadatarecordable_type).to eq('Masterwell')
  end
end
