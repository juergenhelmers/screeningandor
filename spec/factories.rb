###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'faker'
require 'uuidtools'

FactoryGirl.define do
  sequence :email do |n| "person#{n}@example.com" end
  sequence :username do |m| "username#{m}" end

  factory :user do |user|
    email
    username
    user.firstname "John"
    user.lastname "Doe"
    user.password "secret"
    user.approved true
    user.admin    false
  end

  factory :admin, class: User do |admin|
    email
    username
    admin.firstname "John"
    admin.lastname "Doe"
    admin.password "secret"
    admin.approved true
    admin.admin    true
  end

  factory :screen do |screen|
    screen.sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    screen.description  Faker::Lorem.sentence
    association :organism
  end

  factory :invalid_screen, parent: :screen do |f|
    f.name nil
  end

  factory :screen_setting do |screensetting|
    screensetting.substances "Untransfected_infected,Untransfected_infected,MIMcontrol_1,IHcontrol_1,siCD4,siCD4,MIMcontrol_2,IHcontrol_2,uninfected,uninfected"
    screen
  end

  factory :masterplate do |masterplate|
    masterplate.label "test label"
    masterplate.description "sample description"
    masterplate.masterplate_annotation_file "#{Rails.root}/spec/fixtures/files/masterplate.xlsx"
    masterplate.masterplate_sequence_number_offset "1"
    user
    screen
  end

  factory :masterplate_xls, class: Masterplate do |masterplate|
      masterplate.label "test label"
      masterplate.description "sample description"
      masterplate.masterplate_annotation_file "#{Rails.root}/spec/fixtures/files/masterplate.xls"
      masterplate.masterplate_sequence_number_offset "1"
      user
      screen
    end

  factory :masterwell do
    name Faker::Lorem.sentence
    uuid UUIDTools::UUID.timestamp_create().to_s
    association :masterplate
    association :user
  end

  factory :workingwell do
    name Faker::Lorem.sentence
    uuid UUIDTools::UUID.timestamp_create().to_s
    association :workingplate
    association :user
  end

  factory :analytical_result do
    name Faker::Lorem.sentence
    value Faker::Lorem.word
    association :workingwell
  end

  factory :analytical_result_hm, class: AnalyticalResult do
    name "Euclidean Distance"
    value Faker::Number.number(1).to_f.round(4)
    heatmap true
    association :workingwell
  end

  factory :invalid_masterplate, parent: :masterplate do |f|
    f.label nil
  end

  factory :workingplate do |workingplate|
    workingplate.label "test label"
    workingplate.description "sample description"
    association :user
    association :masterplate
  end


  factory :organism do
    sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    description   Faker::Lorem.sentence
  end

  factory :metadata_record do
    sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    value   Faker::Lorem.sentence
    sequence(:position) { |n| n }
    association :metadatarecordable, :factory => :masterwell
  end

  factory :metadata_record_workingwell, class: MetadataRecord do
    sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    value   Faker::Lorem.sentence
    sequence(:position) { |n| n }
    association :metadatarecordable, :factory => :workingwell
  end

  factory :metadata_record_db, class: MetadataRecord do
    sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    value   Faker::Lorem.sentence
    sequence(:position) { |n| n }
    database  true
    url       Faker::Internet.url
  end

  factory :well_comparison do
    description   Faker::Lorem.paragraph
    uuid         UUIDTools::UUID.timestamp_create().to_s
    association :user
  end
end
