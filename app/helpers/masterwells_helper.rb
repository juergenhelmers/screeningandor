###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

module MasterwellsHelper

  def master_popover(well)
    popover = ""
    well.metadata_records.order("position ASC").each do |mr|
      popover << "<strong>#{mr.name}:</strong> #{mr.value} </br>" unless mr.name == ""
    end
    return popover.html_safe
  end

end
