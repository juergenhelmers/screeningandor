###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

module ApplicationHelper

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  def days_until_deadline
    today = Time.now.to_date
    deadline = Date.new(2013, 03, 14)
    return days_until_deadline = (deadline - today).to_i
  end

  def failed_jobs
    string = ""
    fail_count = Resque::Failure.count || 0
    if fail_count > 0
      string << "<span class='red'>#{fail_count}</span>"
    else
      string << "0"
    end
    return string
  end

end
