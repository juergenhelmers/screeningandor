###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

module WorkingplatesHelper

  def return_color_value(value)
    int = value.to_f
    gamma = 1
    if int > 0 && int <= 0.1
      return "rgba(0,52,241,#{gamma})"
    elsif int > 0.1 && int <= 0.2
      return "rgba(0,83,255,#{gamma})"
    elsif int > 0.2 && int <= 0.3
      return "rgba(0,180,255,#{gamma})"
    elsif int > 0.3 && int <= 0.4
      return "rgba(23,255,234,#{gamma})"
    elsif int > 0.4 && int <= 0.5
      return "rgba(136,255,119,#{gamma})"
    elsif int > 0.5 && int <= 0.6
      return "rgba(233,255,25,#{gamma})"
    elsif int > 0.6 && int <= 0.7
      return "rgba(255,164,0,#{gamma})"
    elsif int > 0.7 && int <= 0.8
      return "rgba(255,67,0,#{gamma})"
    elsif int > 0.8 && int <= 0.9
      return "rgba(225,0,0,#{gamma})"
    elsif int > 0.9 && int <= 1
      return "rgba(128,0,0,#{gamma})"
    end
  end

  def heatmap_color_for value # [0,1]
    h = ((1 - value.to_f) * 100).round(2)
    s = 100.round(2)
    l = (value.to_f * 50).round(2)
    return "hsl(#{h},#{s}%,#{l}%)"
  end

  def HeatMapColor(value, min, max)
    val = (value - min) / (max - min)
    r = (255 * val).round(0)
    g = (255 * (1 - val)).round(0)
    b = 0

    return "255,#{r},#{g},#{b}"
  end


  def old_return_color_value(value)
    # transparency value
    gamma = 1
    # define start and end color values

    # start color
    yr = 255
    yg = 0
    yb = 0

    # end color
    xr = 0
    xg = 0
    xb = 255

    # number of color steps
    n = 100

    # maximum value

    max = 100
    # calculate the rounded value in percent
    pos=((value.to_f / max.to_f)*100).round

    # calculate the color values

    red = ( xr + ( pos * (( yr - xr )) / ( n - 1 )))
    green = ( xg + (
    pos * (( yg - xg )) / ( n - 1 )))
    blue = ( xb + ( pos * (( yb - xb )) / ( n - 1 )))




    clr = "rgba(#{red},#{green},#{blue},#{gamma})"

    return clr
  end

end
