###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

module WorkingwellsHelper
  def popover(well)
    if well.montage_url.present?
      url = image_tag url_for(:controller => 'send_images', :action => 'show_file_by_send', :filepath => "#{APP_CONFIG['remote_file_root_directory']}#{well.montage_url}"), :class => "popover_image"
      popover = "#{url} </br>"
    else
    popover = ""
    end
    if well.is_control?
      return popover
    else
      well.metadata_records.order("position ASC").each do |mr|
        popover << "<strong>#{mr.name}:</strong> #{mr.value} </br>" unless mr.name == ""
      end

      return popover.html_safe
    end
  end

  def popover_link(well)
    if well.montage_url.present?
      url = image_tag url_for(:controller => 'send_images', :action => 'show_file_by_send', :filepath => "#{APP_CONFIG['remote_file_root_directory']}#{well.montage_url}"), :class => "popover_image"
      popover = "#{url} </br>"
    else
      popover = ""
    end
    if well.analytical_results.where(name: "Euclidean Distance").present?
      popover << "Euclidean Distance: #{well.analytical_results.where(name: "Euclidean Distance").first.value.to_f.round(4)}"
    else
      popover << ""
    end

    return popover.html_safe

  end


  def metadata_record(well,position)
    mr =  well.metadata_records.order("position ASC")[position] || []
    if mr.present?
      "#{mr.value} <small>(#{mr.name})</small>"
    elsif well.is_control?
      well.name
    else
      "<center>-</center>"
    end
  end

  def euclidean_distance(well)
    ed = "-"
    if well.analytical_results.present? && well.analytical_results.where(name: "Euclidean Distance").present?
      ed = well.analytical_results.where(name: "Euclidean Distance").first.value.to_f.round(4).to_s
    end
    return "<center>#{ed}</center>"
  end

  def list_compare_wells(id)
    well = Workingwell.find(id)
    string = "<tr class='row_#{well.id}'>"
    if well.is_control?
      string << "<td colspan='2'>#{well.name}</td>"
    else
      string << "<td>#{well.name}</td>"
      string << "<td>#{well.metadata_records.order("position ASC").first.value if well.metadata_records.present?}</td>"
    end
    string << "<td class='center'>#{link_to 'x', remove_from_compare_workingwell_path(id), remote: true, class: 'btn btn-mini btn-danger'}</td>"
    string << "</tr>"
    return string  
  end

end
