###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

module WellComparisonsHelper

    def table_euclidean_distance(well)
      if well.analytical_results.where(name: "Euclidean Distance").exists?
        return "<tr><td class='col_one'>Eucl. Distance</td><td>#{well.analytical_results.where(name: "Euclidean Distance").first.value.to_f.round(4).to_s}</td></tr>"
      else
        return "<tr><td>Euclidean Distance</td><td>not calculated</td></tr>"
      end
    end

    def comparison_metadata_records(well)
      return "<tr><td class='col_one'>#{well.metadata_records.order("position ASC").first.name}</td><td>#{well.metadata_records.order("position ASC").first.value}</td></tr>"
    end

    def comparison_control(well)
      return "<tr><td class='col_one'>Control</td><td>#{well.name}</td></tr>"
    end


    def popover_comparison(well_comparison)
      well_ids = well_comparison.workingwell_ids
      if well_ids.length > 0
        string = "<table>"
        Workingwell.find(well_ids).each do |well|
          string << "<tr>"
          if well.is_control?
            string << "<td colspan='2'>#{well.name}</td>"
          else
            string << "<td>#{well.name}</td>"
            string << "<td>#{well.metadata_records.order("position ASC").first.value if well.metadata_records.present?}</td>"
          end
          string << "</tr>"
        end
        string << "</table>"
      else
        string = "<p>No wells found.</p>"
      end
      return string
    end

end
