###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Screen < ActiveRecord::Base
  belongs_to :user
  belongs_to :organism
  has_one :screen_setting, :dependent => :destroy
  has_many :masterplates, :dependent => :destroy
  has_many :analytic_results, :dependent => :destroy

  accepts_nested_attributes_for :screen_setting
  accepts_nested_attributes_for :analytic_results, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true

  attr_accessible :name, :description, :organism_id, :user_id, :screen_setting_attributes, :analytic_results_attributes

  before_create :set_uuid

  validates_presence_of :name, :message => "can't be blank"
  validates_uniqueness_of :name, :message => "a screen with that name does already exist"
  validates :organism, presence: true

private
  def set_uuid
   self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end
end