###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AnalyticalResult < ActiveRecord::Base
  belongs_to :workingwell

  validates :name, presence: true
  validates :value, presence: true

  #scope :with_name_and_value, lambda {|name, value| where(['name = ? AND value = ?', name, value]) }
  #
  #search_methods :with_name_and_value, :splat_param => true, :type => [:string, :string]

  scope :heatmap_data, where(heatmap: true)
  scope :not_heatmap_data, where(heatmap: false)

  scope :with_name_and_value,
        lambda {|name, value|
          where(:name => name, :value => value)
        }

  search_methods :with_name_and_value,
                 :splat_param => true, :type => [:string, :string]

end
