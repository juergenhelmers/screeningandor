###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Workingplate < ActiveRecord::Base
  require 'iconv'
  require 'fileutils'
  require 'uuidtools'

  belongs_to :masterplate
  belongs_to :user
  has_many   :workingwells, :dependent => :destroy

  serialize :htd_file_info, Hash
  
  attr_accessor :mp_id, :image_file
  
  before_create :set_uuid

  scope :with_username, -> { joins(:user).order("workingplates.id").select("workingplates.*,users.username AS user_name") }
  
  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end

  def self.image_type(file)
      if file.present?
        # store file header in string
        command = Escape.shell_command(["#{APP_CONFIG['exiv2']}", '-pa', file])
        result = %x[#{command}]
        if result.include? "Andor"
          return "Andor"
        elsif result.include? "MetaMorph"
          return "MetaMorph"
        else
          return "unknown"
        end
      else
        return "no file given"
      end
  end

  def self.read_htd_file(rfp)
    dir = File.dirname(rfp)                                    # get the directory name from uuid files full path
    raw_htd_file = File.open(Dir[dir + "/**/*.{htd,HTD}"][0])  # read the first HTD file in uuid file's directory
    htd_entry = {}                                             # initialize an empty hash for file content
    raw_htd_file.each do |line|                                # read the file object line by line
      ic = Iconv.new('UTF-8//IGNORE', 'UTF-8')                 # replace invalid utf-8 characters (required!)
      line = ic.iconv(line)
      line.chop!                                               # remove any new line characters
      line.gsub!('"', "").sub!(",", "\t")                      # remove extra quotes and replace only the first comma with a tab
      key, value = line.split("\t")                            # split at tab and assign to key and value
      htd_entry[key] = value                                   # save the key value pair in hash
    end
    return htd_entry                                           # return the complete hash
  end

  def self.get_channel_names(htd_file_info)
    channel_names = {}                                         # instantiate a new has to hold channel names
    htd_file_info.each do |key, value|                         # loop over the htd_file_info hash
      if key.include? "WaveName"                               # check if the key contains "WaveName"
        channel_names[key] = value.gsub(/^\s|\s+$/, "")        # if yes add the new key-value pair to the new hash, remove leading/trailing spaces
      end
    end
    return channel_names
  end
  
  def self.resque_options(dir,workingplate)
    first_image_file = Dir[dir + "/**/*.{tif,TIF}"][0]          # get the first image file from directory
    image_type = self.image_type(first_image_file)              # determine the image_type
    options = {
        "dir" => dir,                                           # image directory
        "workingplate_id" => workingplate.id,                   # workingplate_id
        "rails_env" => Rails.env                                # Rails environment name
    }

    if image_type == "Andor"
      unless workingplate.imported || workingplate.processing
        options['image_type'] = "Andor"                          # image_type
      end
      return options
    elsif image_type == "MetaMorph"
      unless workingplate.imported || workingplate.processing
        options['image_type']      = "MetaMorph"                                        # image_type
        options['rails_env']       = Rails.env                                          # Rails environment name
        options['number_channels'] = workingplate.htd_file_info['NWavelengths'].strip   # save the number of image channels
        options['channel_names']   = self.get_channel_names(workingplate.htd_file_info)  # get the list of channels
      end
      logger.debug "helmerj: options: #{options}"
      return options
    else
      options['image_type'] = "other"
      return options
    end

  end
end
