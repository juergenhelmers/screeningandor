###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellComparison < ActiveRecord::Base
  attr_accessible :description, :user_id, :uuid

  belongs_to :user
  has_many :comparizations
  has_many :workingwells, through: :comparizations

  validates :user_id, presence: true



  before_create :set_uuid



private
  def set_uuid
   self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end

end
