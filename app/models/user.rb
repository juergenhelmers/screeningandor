###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class User < ActiveRecord::Base
  has_many :screens
  has_many :masterplates
  has_many :workingplates
  has_many :masterwells
  has_many :workingwells
  has_many :metadata_records
  has_many :well_comparisons

  after_create :send_welcome_email
  #before_save :titleize_names


  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable, :rememberable
  devise :database_authenticatable, :registerable, :recoverable, :trackable, :validatable
  validates_uniqueness_of :username, message: "has already been taken."

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :username, :firstname, :lastname, :password_confirmation, :remember_me, :admin, :approved, :receives_system_email
  
  def active_for_authentication? 
    super && approved? 
  end

  def fullname
    [self.firstname, self.lastname].join(" ")
  end
  
  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end

  private

  def titleize_name
    self.firstname.titleize
    self.lastname.titleize
  end

  def send_welcome_email
    UserMailer.deliver_welcome_email(self).deliver
    admin = User.where(:admin => true).where(:receives_system_email => true)
    UserMailer.deliver_signup_notification_email(admin,self).deliver
  end

  
end
