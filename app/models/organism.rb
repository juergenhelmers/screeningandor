###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Organism < ActiveRecord::Base
  attr_accessible :description, :name

  has_many :screens

  validates :name, presence: true, uniqueness: true

end
