###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Comparization < ActiveRecord::Base
  attr_accessible :well_comparison_id, :workingwell_id

  belongs_to :workingwell
  belongs_to :well_comparison

end
