###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Workingwell < ActiveRecord::Base

  belongs_to :workingplate
  belongs_to :user

  has_many :metadata_records, as: :metadatarecordable, :dependent => :destroy
  has_many :analytical_results, :dependent => :destroy

  has_many :comparizations
  has_many :workingwells, through: :comparizations

  validates :name, presence: true
  validates :uuid, presence: true

  #scope :with_name_and_value_range,lambda {|name, low, high| joins(:analytical_results).where(:analytical_results => {:name => name, :value => low..high}) }
  #scope :with_name_and_value_range,lambda {|name, low, high|
  #joins(:analytical_results).where(['analytical_results.name = ?', name]).where(['analytical_results.value >= ?', low]).where(['analytical_results.value <= ?', high])}
  default_scope order('id ASC')

  scope :with_name_and_value_range,lambda {|name, low, high|
    joins(:analytical_results).where(['analytical_results.name = ?', name]).with_low(low).with_high(high) }
  scope :with_name,lambda {|name|
    joins(:analytical_results).where(['analytical_results.name = ?', name])}
  scope :with_low,lambda {|low|
    joins(:analytical_results).where(['analytical_results.value >= ?', low]) unless low.blank?}
  scope :with_high,lambda {|high|
    joins(:analytical_results).where(['analytical_results.value <= ?', high]) unless high.blank?}

  search_methods :with_name_and_value_range,
    :splat_param => true, :type => [:string, :string, :string]

  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end

  def self.to_csv(options = {col_sep: ";"})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |well|
        csv << well.attributes.values_at(*column_names)
      end
    end
  end
end
