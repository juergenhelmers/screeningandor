###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class ScreenSetting < ActiveRecord::Base
  belongs_to :screen

  validates :substances, :presence => true, :substance_format => true

end
