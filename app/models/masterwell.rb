###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Masterwell < ActiveRecord::Base
  belongs_to :masterplate
  belongs_to :user
  has_many :metadata_records, as: :metadatarecordable, :dependent => :destroy


  before_create :set_uuid

  validates :name, presence: true
  validates :uuid, presence: true

  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end
end
