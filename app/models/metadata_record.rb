###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class MetadataRecord < ActiveRecord::Base
  attr_accessible :database, :name, :url, :uuid, :value, :user_id, :metadatarecordable_id, :metadatarecordable_type, :position

  belongs_to :metadatarecordable, polymorphic: true
  belongs_to :user

  validates :name, presence: true
  validates :value, presence: true
  validates :position, presence: true
  validates :url, presence: true, if: :database?

  def database?
    self.database
  end

end
