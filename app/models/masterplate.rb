###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class Masterplate < ActiveRecord::Base
  
  belongs_to :screen
  belongs_to :user
  has_many :masterwells, :dependent => :destroy
  has_many :workingplates, :dependent => :destroy
  
  before_create :set_uuid
  
  #create virtual attribute for file name
  attr_accessor :masterplate_annotation_file, :masterplate_sequence_number_offset
 
  validates_presence_of :label, :on => :create, :message => "can't be blank"
  validates_presence_of :masterplate_annotation_file, :on => :create, :message => "can't be blank"

  # scopes
  scope :with_user_and_screen_name, -> { joins(:user).joins(:screen).order("masterplates.id").select("masterplates.*,users.username AS user_name, screens.name AS screen_name") }
 
  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end

end
