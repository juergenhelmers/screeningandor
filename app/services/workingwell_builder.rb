class WorkingwellBuilder

  def initialize workingplate_id,active_wells,activewell_offset,current_user_id
    @workingplate_id   = workingplate_id
    @active_wells      = active_wells
    @activewell_offset = activewell_offset
    @current_user_id   = current_user_id
  end

  def build
    substances = @active_wells.first.masterplate.screen.screen_setting.substances.split(",")
    column_count = 0
    row_count = 0
    insert_workingwells = []        # create empty insert_workingwells array for sql_query
    insert_metadata_records = []    # create empty insert_metadata_records array for sql_query to add associated metadata records

    while row_count < 8 do
      if row_count == 0 || row_count == 7
        12.times do                                         # first row is PBS
          uuid = UUIDTools::UUID.timestamp_create().to_s
          insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'PBS',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
        end
        row_count = row_count + 1
      elsif row_count == 1 || row_count == 4                # rows 1 and 4 are controls
        uuid = UUIDTools::UUID.timestamp_create().to_s      # first column is filled w/ PBS
        insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'PBS',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
        substances_counter = 0
        while substances_counter < 10 do
          uuid = UUIDTools::UUID.timestamp_create().to_s    # loop over control substances and create wells
          insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{substances[substances_counter]}',#{@workingplate_id},#{@current_user_id},'#{uuid}','t')")
          substances_counter += 1
        end
        uuid = UUIDTools::UUID.timestamp_create().to_s
        insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'PBS',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
        row_count = row_count + 1
      else
        while column_count < 12
          if column_count == 0 || column_count == 11        # first column is always PBS
            uuid = UUIDTools::UUID.timestamp_create().to_s
            insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'PBS',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
          elsif @active_wells[@activewell_offset].present?
            uuid = UUIDTools::UUID.timestamp_create().to_s
            # TODO link to existing masterwell associations to prevent the duplication of metadata-records
            insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{@active_wells[@activewell_offset].name}',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
            @active_wells[@activewell_offset].metadata_records.each do |metadata_record|
              insert_metadata_records.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{uuid}','#{@current_user_id}','#{metadata_record.name}', '#{metadata_record.value}', '#{metadata_record.position}')")
            end
            @activewell_offset += 1
          else
            uuid = UUIDTools::UUID.timestamp_create().to_s
            insert_workingwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'empty',#{@workingplate_id},#{@current_user_id},'#{uuid}','f')")
          end
          column_count = column_count + 1
        end
        column_count = 0
        row_count = row_count + 1
      end

    end

    sql_query_workingwells   = "INSERT INTO workingwells (created_at,updated_at,name,workingplate_id,user_id,uuid,is_control) VALUES #{insert_workingwells.join(", ")}"
    sql_metadata_records  = "INSERT INTO metadata_records (created_at, updated_at, uuid, user_id, name, value, position) VALUES #{insert_metadata_records.join(", ")}"

    execute_sql sql_query_workingwells
    execute_sql sql_metadata_records

    update_workingwells

  end


  private

  def execute_sql query
    ActiveRecord::Base.connection.execute query
  end

  def update_workingwells
    Workingwell.where(workingplate_id: "#{@workingplate_id}").each do |well|
      MetadataRecord.where(uuid: "#{well.uuid}").update_all(metadatarecordable_id: "#{well.id}", metadatarecordable_type: "Workingwell")
    end
  end


end
