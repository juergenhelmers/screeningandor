class MasterwellBuilder


  def initialize masterplate_id,line_offset,lines_max,header,spreadsheet,current_user_id
    @masterplate_id  = masterplate_id
    @line_offset     = line_offset
    @lines_max       = lines_max
    @header          = header
    @spreadsheet     = spreadsheet
    @current_user_id = current_user_id
  end

  def build
    # create masterwells by looping over the annotation file and fill each plate with max 80 wells
    # first and last column will be filled with empty wells, aka "PBS"
    #
    # Instead of using activerecord to create dependent wells, a single SQL statement is generated and used to create all wells
    # in a single database query
    # NOTE: the current statement created is only working with PostgesSQL!!
    #
    row_count = 0
    column_count = 1
    row_character = ('A'..'H').to_a
    line_counter = @line_offset
    insert_masterwells      = []
    insert_metadata_records = []

    while row_count < 8 do
      while column_count < 13 do
        if column_count == 1 || column_count == 12  # first and last well is empty
          if line_counter < @lines_max
            uuid = UUIDTools::UUID.timestamp_create().to_s
            insert_masterwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{@masterplate_id}','#{uuid}',#{@current_user_id},'#{row_character[row_count]}#{sprintf('%02i',column_count)}')")
          end
        else
          if line_counter < @lines_max
            line = Hash[[@header, @spreadsheet.row(line_counter)].transpose]
            if line.to_a[0][1].present?      # check for value for first key of line hash
              uuid = UUIDTools::UUID.timestamp_create().to_s
              insert_masterwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{@masterplate_id}','#{uuid}',#{@current_user_id},'#{row_character[row_count]}#{sprintf('%02i',column_count)}')")
              line.each_with_index do |(key,value),index|
                unless key == "Plate" || key == "Well" || key == ""     # do not add redundant plate, well fields (are saved with well already) and do not add values from columns with not header
                  insert_metadata_records.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{uuid}', '#{@current_user_id}', '#{key}', '#{value}','#{index}')")
                end
              end
              line_counter += 1
            end
          else
            line_counter += 1
          end

        end
        column_count += 1
      end
      column_count = 1
      row_count += 1
    end
    sql_query_masterwells = "INSERT INTO masterwells (created_at,updated_at,masterplate_id,uuid,user_id,name) VALUES #{insert_masterwells.join(", ")}"
    sql_query_metadata    = "INSERT INTO metadata_records (created_at, updated_at, uuid, user_id, name, value,position) VALUES #{insert_metadata_records.join(", ")}"
    execute_sql sql_query_masterwells
    execute_sql sql_query_metadata
    update_masterwells

  end


  private

  def execute_sql query
    ActiveRecord::Base.connection.execute query
  end

  def update_masterwells
    Masterwell.where(masterplate_id: "#{@masterplate_id}").each do |well|
      MetadataRecord.where(uuid: "#{well.uuid}").update_all(metadatarecordable_id: "#{well.id}", metadatarecordable_type: "Masterwell")
    end
  end

end
