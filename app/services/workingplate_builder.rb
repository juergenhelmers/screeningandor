class WorkingplateBuilder

  def initialize params, current_user_id
    @params          = params
    @current_user_id = current_user_id
  end

  def build
    @masterplate     = Masterplate.find_by_id(@params[:workingplate][:mp_id])
    @active_wells = []
    @masterplate.masterwells.each do |masterwell|                           # get the well with a metadata_record attached to them
      @active_wells << masterwell if masterwell.metadata_records.present?
    end


    masterwell_number = @active_wells.length                                # save the number of used wells
    if masterwell_number <= 40                                              # calculate the number of working plates to create, btw 1 and 2
      workingplate_number = 1
    else
      workingplate_number = 2
    end
    # workingplate_number = masterwell_number <=40 ? 1:2

    label_suffix = ('A'..'ZZ').to_a                                          # initialize the array for label suffix
    number_of_workingplates_offset = @masterplate.workingplates.length
    label_suffix_count =  number_of_workingplates_offset
    workingplate_count = 0
    activewell_offset = 0

    while workingplate_count < workingplate_number do
      @workingplate = Workingplate.new(@params[:workingplate])                         # create a new workingplate object
      @workingplate.update_attributes(:user_id => @current_user_id,                    # assign user_id, masterplate_id and new label
                                      :masterplate_id => @masterplate.id,
                                      :label => "#{@masterplate.label} - #{label_suffix[label_suffix_count]}")
      @workingplate.save

      ::WorkingwellBuilder.new(@workingplate.id,@active_wells,activewell_offset,@current_user_id).build
      # update loop counters
      workingplate_count += 1
      label_suffix_count += 1
      activewell_offset  += 40
    end
    return @workingplate
  end


  private


end
