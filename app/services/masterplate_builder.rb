class MasterplateBuilder

  def initialize params, current_user_id
    @params = params
    @current_user_id = current_user_id
    @spreadsheet = open_spreadsheet_file(spreadsheet_file)
    @spreadsheet.default_sheet = @spreadsheet.sheets.first
    @header = @spreadsheet.row(1) # assign the first row of the spreadsheet to be the header
  end


  def build
    sequence_number = @params[:masterplate][:masterplate_sequence_number_offset].to_i
    plate_count = 1
    spreadsheet_lines_max = @spreadsheet.last_row + 1
    inputfile_line_offset = 2                                                         # the first entries are in line
    max_plate_number = ((spreadsheet_lines_max - inputfile_line_offset -1 )/80) + 1   # calculate the maximum number of masterplates to create

    while plate_count <= max_plate_number
      masterplate = Masterplate.new(@params[:masterplate])
      # update the label for the plate with  running number
      masterplate.update_attributes(:label => "#{masterplate.label}" + "-"+ "#{sprintf('%02i', sequence_number)}", :user_id => @current_user_id)
      masterplate.save
      ::MasterwellBuilder.new(masterplate.id,inputfile_line_offset,spreadsheet_lines_max, @header, @spreadsheet,@current_user_id).build
      plate_count += 1
      sequence_number += 1
      inputfile_line_offset += 80
    end
    return masterplate
  end

  private

  def spreadsheet_file
    @params[:masterplate][:masterplate_annotation_file]
  end

  def open_spreadsheet_file(file)
    case File.extname(file.original_filename)
      when '.xls' then Roo::Spreadsheet.open(file.path, extension: :xls)
      when '.xlsx' then Roo::Spreadsheet.open(file.path, extension: :xlsx)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
