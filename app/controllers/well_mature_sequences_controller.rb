###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellMatureSequencesController < ApplicationController
  def index
    @mature_sequences = Workingwell.order(:mature_sequence).where("mature_sequence ilike ?", "%#{params[:term]}%")
    render json: @mature_sequences.map(&:mature_sequence)
  end
end