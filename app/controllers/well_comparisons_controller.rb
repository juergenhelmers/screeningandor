###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellComparisonsController < ApplicationController
  layout "full_with"
  before_filter :authenticate_user!

  # GET /well_comparisons
  # GET /well_comparisons.json
  def index
    if params[:me] == "true"
      @well_comparisons = WellComparison.where(:user_id => current_user.id)
    else
      @well_comparisons = WellComparison.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @well_comparisons }
    end
  end

  # GET /well_comparisons/1
  # GET /well_comparisons/1.json
  def show
    @well_comparison = WellComparison.find(params[:id])
    # render layout: "full_with"
  end

  # GET /well_comparisons/new
  # GET /well_comparisons/new.json
  def new
    @well_comparison = WellComparison.new
    # render layout: "full_with"
    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @well_comparison }
    # end
  end

  # GET /well_comparisons/1/edit
  def edit
    @well_comparison = WellComparison.find(params[:id])
  end

  # POST /well_comparisons
  # POST /well_comparisons.json
  def create
    if session[:compare].length > 0
      @well_comparison = WellComparison.new(params[:well_comparison])
      @well_comparison.update_attributes(user_id: current_user.id)
      @well_comparison.workingwell_ids = session[:compare].values

      respond_to do |format|
        if @well_comparison.save
          session[:compare] = {}
          format.html { redirect_to @well_comparison, notice: 'Well Comparison was successfully saved.' }
          format.json { render json: @well_comparison, status: :created, location: @well_comparison }
        else
          format.html { render action: "new" }
          format.json { render json: @well_comparison.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to new_well_comparison_path, notice: 'You have to include at least one well in a comparison.'
    end

  end

  # PUT /well_comparisons/1
  # PUT /well_comparisons/1.json
  def update
    @well_comparison = WellComparison.find(params[:id])

    respond_to do |format|
      if @well_comparison.user_id == current_user.id || current_user.admin?
        if @well_comparison.update_attributes(params[:well_comparison])
          format.html { redirect_to @well_comparison, notice: 'Well comparison was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @well_comparison.errors, status: :unprocessable_entity }
        end
      else
        redirect_to @well_comparison, notice: 'This Well Comparison does nto belong to you.'
      end

    end
  end

  # DELETE /well_comparisons/1
  # DELETE /well_comparisons/1.json
  def destroy
    @well_comparison = WellComparison.find(params[:id])
    if @well_comparison.user_id == current_user.id || current_user.admin?
      @well_comparison.destroy

      respond_to do |format|
        format.html { redirect_to well_comparisons_url }
        format.json { head :no_content }
      end
    else
      redirect_to @well_comparison, notice: 'This Well Comparison does not belong to you.'
    end
  end

  def compare_with_control
    @workingwell = Workingwell.find(params[:id])
    @control_wells = @workingwell.workingplate.workingwells.where(is_control: true ).limit(10)

  end

end
