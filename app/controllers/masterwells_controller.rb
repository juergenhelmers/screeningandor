###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class MasterwellsController < ApplicationController
  # GET /masterwells
  # GET /masterwells.json
  before_filter :authenticate_user!
  
  

  # GET /masterwells/1
  # GET /masterwells/1.json
  def show
    @masterwell = Masterwell.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @masterwell }
    end
  end

  # GET /masterwells/new
  # GET /masterwells/new.json
  def new
    @masterwell = Masterwell.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @masterwell }
    end
  end

  # GET /masterwells/1/edit
  def edit
    @masterwell = Masterwell.find(params[:id])
  end

  # POST /masterwells
  # POST /masterwells.json
  def create
    @masterwell = Masterwell.new(params[:masterwell])
    @masterwell.update_attributes(:user_id => current_user.id)
    
    respond_to do |format|
      if @masterwell.save
        format.html { redirect_to @masterwell, notice: 'Masterwell was successfully created.' }
        format.json { render json: @masterwell, status: :created, location: @masterwell }
      else
        format.html { render action: "new" }
        format.json { render json: @masterwell.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /masterwells/1
  # PUT /masterwells/1.json
  def update
    @masterwell = Masterwell.find(params[:id])

    respond_to do |format|
      if @masterwell.update_attributes(params[:masterwell])
        format.html { redirect_to @masterwell, notice: 'Masterwell was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @masterwell.errors, status: :unprocessable_entity }
      end
    end
  end


end
