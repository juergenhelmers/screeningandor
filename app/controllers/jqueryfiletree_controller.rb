###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class JqueryfiletreeController < ApplicationController
  
  before_filter :authenticate_user!
  
  #protect_from_forgery :only => []
  
  def content
    @parent = params[:dir]
    @dir = Jqueryfiletree.new("#{APP_CONFIG['remote_file_root_directory']}/"+@parent).get_content
  end
  
end