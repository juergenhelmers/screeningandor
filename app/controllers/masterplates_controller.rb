###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class MasterplatesController < ApplicationController
  # GET /masterplates
  # GET /masterplates.json
  

  
  before_filter :authenticate_user!
  
  
  def index
    if params[:me] == "true"
      @masterplates =Masterplate.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => 20)
    else
      @masterplates = Masterplate.all.paginate(:page => params[:page], :per_page => 20)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @masterplates }
    end
  end

  # GET /masterplates/1
  # GET /masterplates/1.json
  def show
    @masterplate = Masterplate.joins(:screen).select("masterplates.*, screens.name AS screen_name, screens.id AS screen_id").includes(:masterwells => :metadata_records).find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @masterplate }
    end
  end

  # GET /masterplates/new
  # GET /masterplates/new.json
  def new
    if current_user.screens.present?
      @masterplate = Masterplate.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @masterplate }
      end
    else
      redirect_to new_screen_path, notice: "You need to create a screen before creating masterplates."
    end
  end

  # GET /masterplates/1/edit
  def edit
    @masterplate = Masterplate.find(params[:id])
  end

  # POST /masterplates
  # POST /masterplates.json
  def create
    if params[:masterplate][:masterplate_annotation_file].present? && params[:masterplate][:label].present? &&  params[:masterplate][:screen_id].present?
      Rails.logger.info("PARAMS: #{params}")
      @masterplate = MasterplateBuilder.new(params, current_user.id).build
    else
      message = ["The following fields have to be specified:"]
      if params[:masterplate][:masterplate_annotation_file].blank?
        message << "Spreadsheet input file"
      end
      if params[:masterplate][:label].blank?
        message << "Label root"
      end
      if params[:masterplate][:screen_id].blank?
        message << "Screen"
      end
      redirect_to new_masterplate_path, alert: "#{message.join(", ")}." and return
    end



    respond_to do |format|
      if @masterplate.save
        format.html { redirect_to masterplates_path(:me => true), notice: 'Masterplates successfully created.' }
        format.json { render json: @masterplate, status: :created, location: @masterplate }
      else
        format.html { render action: "new" }
        format.json { render json: @masterplate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /masterplates/1
  # PUT /masterplates/1.json
  def update
    @masterplate = Masterplate.find(params[:id])

    respond_to do |format|
      if @masterplate.user_id == current_user.id || current_user.admin? 
         if @masterplate.update_attributes(params[:masterplate])
          format.html { redirect_to @masterplate, notice: 'Masterplate was successfully updated.', :me => true }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @masterplate.errors, status: :unprocessable_entity }
        end  
      else
        format.html { render action: "edit", error: "You are not allowed to edit this plate."  }
      end
     
    end
  end

  # DELETE /masterplates/1
  # DELETE /masterplates/1.json
  def destroy
    @masterplate = Masterplate.find(params[:id])
    if @masterplate.user_id == current_user.id || current_user.admin?
      @masterplate.destroy
  
      respond_to do |format|
        format.html { redirect_to masterplates_url, :me => true }
        format.json { head :no_content }
      end
    
    else
      format.html {redirect_to @masterplate, error: "You are not allowed to delete this plate."}  
    end

  end
  




end
