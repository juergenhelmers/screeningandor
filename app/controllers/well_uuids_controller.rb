###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellUuidsController < ApplicationController
  def index
    @uuids = Workingwell.order(:uuid).where("uuid ilike ?", "%#{params[:term]}%")
    render json: @uuids.map(&:uuid)
  end
end
