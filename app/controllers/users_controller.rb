###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class UsersController < ApplicationController
  # GET /users
  # GET /users.json

  before_filter :authenticate_user!
  before_filter :admin_required, :except => [:update, :edit]

  def index
    if params[:approved] == "false"
      @users = User.where(:approved => false).order(:created_at)
    else
      @users = User.order(:username)
    end
  end



  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  def approve       # custom action to approve account
    @user = User.find(params[:id])
    if current_user && current_user.admin?
      @user.approved = true
      if @user.save
        UserMailer.account_approval(@user).deliver
        redirect_to users_path, :notice => "User has been approved"
      end


    end

  end

  def disable        # custom action to disable account
    @user = User.find(params[:id])
    if current_user && current_user.admin?
      @user.approved = false
      @user.save
      redirect_to users_path, :notice => "User has been disabled"
    end
  end

  def grant_admin   # custom action to grant admin status
    @user = User.find(params[:id])
    if current_user && current_user.admin?
      @user.admin = true
      @user.save
      redirect_to users_path, :notice => "User has been granted admin status"
    end
  end

  def deny_admin     # custom action to deny admin status
    @user = User.find(params[:id])
    if current_user && current_user.admin?
      @user.admin = false
      @user.save
      redirect_to users_path, :notice => "User has been denied admin status"
    end
  end

  def grant_receive_system_mail   # custom action to grant admin status
    @user = User.find(params[:id])
    if current_user && current_user.admin? && @user.admin?
      @user.receives_system_email = true
      @user.save
      redirect_to users_path, :notice => "User will receive system emails in the future"
    end
  end

  def deny_receive_system_mail     # custom action to deny admin status
    @user = User.find(params[:id])
    if current_user && current_user.admin?
      @user.receives_system_email = false
      @user.save
      redirect_to users_path, :notice => "User will no longer receive system emails"
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if current_user && (current_user.id == @user.id || current_user.admin?)
        logger.debug "devise: authenticated to update"
        @user.attributes = (params[:user])
        if @user.save
          logger.debug "devise: updated"
          format.html { redirect_to users_path, notice: 'User was successfully updated.' }
          format.json { head :no_content }
        else
          logger.debug "devise: update failed #{params}"
          format.html { render action: "edit" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        logger.debug "devise: not authenticated"
        format.html { render action: "edit", notice: "You are not allowed to edit this user." }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    if current_user && @user.id == current_user.id || current_user.admin?
      @user.destroy

      respond_to do |format|
        format.html { redirect_to users_url }
        format.json { head :no_content }
      end
    else
      format.html {redirect_to users_url, error: "You are not allowed to delete this user."}
    end
  end
end
