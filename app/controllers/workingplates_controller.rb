###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WorkingplatesController < ApplicationController
  # GET /workingplates
  # GET /workingplates.json
  require 'rexml/document'
  require 'uuidtools'
  require 'fileutils'


  before_filter :authenticate_user!
  
  def index
    if params[:term].present?    # for autosuggest look up
      @workingplates = Workingplate.order(:label).where("label ilike ?", "%#{params[:term]}%")
      render json: @workingplates.map(&:label)
    else
      if params[:me] == "true"
        @workingplates = Workingplate.with_username.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => 20)
      elsif params[:status] == "imported"
        @workingplates = Workingplate.with_username.where("user_id = ? AND imported = ?", "#{current_user.id}", true ).paginate(:page => params[:page], :per_page => 20)
      elsif params[:status] == "processing"
        @workingplates = Workingplate.with_username.where("user_id = ? AND processing = ?", "#{current_user.id}", true ).paginate(:page => params[:page], :per_page => 20)
      elsif params[:status] == "unprocessed"
        @workingplates = Workingplate.with_username.where("user_id = ? AND processing = ? AND imported =?", "#{current_user.id}", false, false ).paginate(:page => params[:page], :per_page => 20)
      else
        @workingplates = Workingplate.with_username.paginate(:page => params[:page], :per_page => 20)
      end
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @workingplates }
      end

    end
  end

  # GET /workingplates/1
  # GET /workingplates/1.json
  def show
    # @workingplate = Workingplate.find(params[:id])
    @workingplate = Workingplate.joins(:user).joins(:masterplate).select("workingplates.*, users.username AS user_name, masterplates.label AS masterplate_label").includes(:workingwells => :metadata_records).find(params[:id])
    @htd = @workingplate.htd_file_info.to_a
    # @workingwells = @workingplate.workingwells.order("id ASC")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @workingplate }
    end
  end

  # GET /workingplates/new
  # GET /workingplates/new.json
  def new
    @workingplate = Workingplate.new
    @masterplate = Masterplate.find_by_id(params[:mp_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workingplate }
    end
  end

  # GET /workingplates/1/edit
  def edit
    @workingplate = Workingplate.find(params[:id])
    @masterplate = Masterplate.find_by_id(@workingplate.masterplate_id)
  end

  def labels
    @workingplate_labels = Workingplate.order(:label).where("label like = ?", "%#{params[:term]}%")
    render json: @workingplate_labels.map(&:label)
  end

  # POST /workingplates
  # POST /workingplates.json
  def create
    # get the parent masterplate object
    @workingplate = WorkingplateBuilder.new(params, current_user.id).build

    respond_to do |format|
      if @workingplate.save
        format.html { redirect_to @workingplate, notice: 'Workingplates successfully created.' }
        format.json { render json: @workingplate, status: :created, location: @workingplate }
      else
        format.html { render action: "new" }
        format.json { render json: @workingplate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /workingplates/1
  # PUT /workingplates/1.json
  def update
    @workingplate = Workingplate.find(params[:id])
    @masterplate = Masterplate.find_by_id(@workingplate.masterplate_id)

    respond_to do |format|
      if @workingplate.update_attributes(params[:workingplate])
        format.html { redirect_to @workingplate, notice: 'Workingplate was successfully updated.', :me => true }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        @masterplate = Masterplate.find_by_id(@workingplate.masterplate_id)
        format.json { render json: @workingplate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workingplates/1
  # DELETE /workingplates/1.json
  def destroy
    @workingplate = Workingplate.find(params[:id])
    @workingplate.destroy

    respond_to do |format|
      format.html { redirect_to workingplates_url, :me => true }
      format.json { head :no_content }
    end
  end


  # GET /workingplates/1/omeroimport
  def omeroimport
    # change permissions to own all files by group rubyonrails to make them accessible to the application

    # rename all directories in the root file tree to replace white-spaces with underscores
    # jqueryfiletree will crash otherwise
    Dir["#{APP_CONFIG['remote_file_root_directory']}" + "/**/"].each {|x| File.rename x, x.gsub(" ", "_") if File.directory? x}
    @workingplate = Workingplate.find(params[:id])
  end

  def import
    @workingplate = Workingplate.find(params[:id])
    @rfp = params[:workingplate][:remote_file_path]                     # get the file path and UUID file name from form field
    file_uuid = File.basename(@rfp).gsub(".uuid", "")                   # remove the suffix => UUID
    dir = File.dirname(@rfp)                                            # get the full path
    if file_uuid.present? && @workingplate.uuid == file_uuid            # in case of UUID and if UUID matches that of workingplate
      htd_file_info = Workingplate.read_htd_file(@rfp)
      @workingplate.update_attributes(:remote_file_path => @rfp,        # save the full path to workingplate record
                                      :htd_file_info => htd_file_info)  # save the htd file information hash
      @workingplate.save                                                # save the updated record

      options = Workingplate.resque_options(dir,@workingplate)

      unless options['image_type'] == "other"
        logger.debug "helmerj: options before worker: #{options}"
        Resque.enqueue(RewriteOmeXml, options)                    # enqueue the new job
        @workingplate.update_attributes(:processing => 1)         # set the processing flag
        @workingplate.save                                        # save the update
        respond_to do |format|
          format.html { redirect_to @workingplate, notice: "Image files have been successfully added to the processing queue." }
          format.json { head :no_content }
        end
      else
        redirect_to omeroimport_workingplate_path, :alert => "The image files in the source directory are neither Andor nor MetaMorph files!" and return
      end
    else
      redirect_to omeroimport_workingplate_path, :alert => "#{@rfp} The Workingplate UUID does not match the UUID from the image directory or no UUID file was chosen." and return
    end
  end

  def generate_uuid_file
    @workingplate = Workingplate.find(params[:id])                        # get the current workingplate
    file = ''                                                             # instantiate a new file
    file << @workingplate.uuid                                            # use the workingplate UUID as file name
    send_data file, :filename => @workingplate.uuid, :type => 'text/uuid' # send the file to the browser
  end

end

