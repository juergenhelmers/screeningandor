###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class AnalyticResultsController < ApplicationController
  # GET /analytic_results
  # GET /analytic_results.json
  def index
    @analytic_results = AnalyticResult.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @analytic_results }
    end
  end

  # GET /analytic_results/1
  # GET /analytic_results/1.json
  def show
    @analytic_result = AnalyticResult.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @analytic_result }
    end
  end

  # GET /analytic_results/new
  # GET /analytic_results/new.json
  def new
    @analytic_result = AnalyticResult.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @analytic_result }
    end
  end

  # GET /analytic_results/1/edit
  def edit
    @analytic_result = AnalyticResult.find(params[:id])
  end

  # POST /analytic_results
  # POST /analytic_results.json
  def create
    @analytic_result = AnalyticResult.new(params[:analytic_result])

    respond_to do |format|
      if @analytic_result.save
        format.html { redirect_to @analytic_result, notice: 'Analytic result was successfully created.' }
        format.json { render json: @analytic_result, status: :created, location: @analytic_result }
      else
        format.html { render action: "new" }
        format.json { render json: @analytic_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /analytic_results/1
  # PUT /analytic_results/1.json
  def update
    @analytic_result = AnalyticResult.find(params[:id])

    respond_to do |format|
      if @analytic_result.update_attributes(params[:analytic_result])
        format.html { redirect_to @analytic_result, notice: 'Analytic result was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @analytic_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /analytic_results/1
  # DELETE /analytic_results/1.json
  def destroy
    @analytic_result = AnalyticResult.find(params[:id])
    @analytic_result.destroy

    respond_to do |format|
      format.html { redirect_to analytic_results_url }
      format.json { head :no_content }
    end
  end
end
