###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class SendImagesController < ApplicationController

  def show_file_by_send
    send_file params[:filepath],
    :type => 'image/png',
    :disposition => "inline"
  end

end
