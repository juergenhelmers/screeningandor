###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class ApplicationController < ActionController::Base
  protect_from_forgery
  
  require 'will_paginate/array'

  def admin_required
    current_user.admin? || redirect_to("/")
  end
  
  
  
end
