###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class ScreensController < ApplicationController
  # GET /screens
  # GET /screens.json
  
  before_filter :authenticate_user!
  
  def index
    if params[:me] == "true"
      @screens = Screen.joins(:user).select("screens.*, users.username AS user_name").where(:user_id => current_user.id)
    else
      @screens = Screen.joins(:user).select("screens.*, users.username AS user_name")
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @screens }
    end
  end

  # GET /screens/1
  # GET /screens/1.json
  def show
    @screen = Screen.joins(:user).joins(:organism).select("screens.*, users.username AS user_name, organisms.name AS organism_name, organisms.description AS organism_description").includes(:masterplates).includes(:screen_setting).find(params[:id])
    #@masterplates = @screen.masterplates
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @screen }
    end
  end

  # GET /screens/new
  # GET /screens/new.json
  def new
    @screen = Screen.new
    @screen.build_screen_setting  # instantiate dependent record

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @screen }
    end
  end

  # GET /screens/1/edit
  def edit
    @screen = Screen.find(params[:id])
  end

  # POST /screens
  # POST /screens.json
  def create
    @screen = Screen.new(params[:screen])
    @screen.update_attributes(:user_id => current_user.id)
    respond_to do |format|
      if @screen.save
        format.html { redirect_to @screen, notice: 'Screen was successfully created.', :me => true }
        format.json { render json: @screen, status: :created, location: @screen }
      else
        format.html { render action: "new" }
        format.json { render json: @screen.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /screens/1
  # PUT /screens/1.json
  def update
    @screen = Screen.find(params[:id])

    respond_to do |format|
      if current_user && current_user.id == @screen.user_id || current_user.admin?
        if @screen.update_attributes(params[:screen])
          format.html { redirect_to @screen, notice: 'Screen was successfully updated.', :me => true }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @screen.errors, status: :unprocessable_entity }
        end
      else
        format.html { render action: "edit", notice: "You are not allowed to edit this screen." }
      end
    end
  end

  # DELETE /screens/1
  # DELETE /screens/1.json
  def destroy
    @screen = Screen.find(params[:id])
    if current_user && @screen.user_id == current_user.id || current_user.admin?
      @screen.destroy

      respond_to do |format|
        format.html { redirect_to screens_url, :me => true }
        format.json { head :no_content }
      end
    else
      format.html {redirect_to screens_url, error: "You are not allowed to delete this screen."}
    end
  end
end
