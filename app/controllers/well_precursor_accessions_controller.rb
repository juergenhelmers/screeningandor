###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellPrecursorAccessionsController < ApplicationController
  def index
    @precursor_accessions = Workingwell.order(:precursor_accession).where("precursor_accession ilike ?", "%#{params[:term]}%")
    render json: @precursor_accessions.map(&:precursor_accession)
  end
end
