###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WorkingwellsController < ApplicationController
  # GET /workingwells
  # GET /workingwells.json
  # require 'csv'

  before_filter :authenticate_user!

  def index
    session[:compare] = {} unless session[:compare].present?
    @screens = Screen.order("name ASC")#.collect{|x| x.name}
    @users = User.where("username != ?", "admin").order("username ASC")#collect{|x| x.username}
    @analytical_results = AnalyticalResult.select(:name).uniq if AnalyticalResult.all.size > 0
    new = {}                                        # instantiate a new has to hold extra values
    if params[:search].present?
      params[:search].each do |key, value|            # loop over the params[:search] hash
        if key.include? "with_name_and_value_range"   # check if the key contains "with_name_and_value_range"
          new[key] = value                            # if yes add the new key-value pair to the new hash
          params[:search].delete(key)                 # remove the key-value pair from the params[:search] hash
        end
      end
    else
      params[:search] = {}
    end
    @search = Workingwell.includes(:analytical_results).where("workingwells.name != ? AND workingwells.name != ?", "PBS", "empty")
    if new.length > 0
      new_a = new.to_a.each_slice(3).to_a             # convert new has into an 2d array
      count = 0
      while count < new_a.length
        if new_a[count][1][1] != ""
          @search = @search.joins(:analytical_results).where("analytical_results.name = ? AND analytical_results.value >= ?", "#{new_a[count][0][1]}", "#{new_a[count][1][1].to_f}")
        end
        if new_a[count][2][1] != ""
          @search = @search.joins(:analytical_results).where("analytical_results.name = ? AND analytical_results.value <= ?", "#{new_a[count][0][1]}", "#{new_a[count][2][1].to_f}")
        end
        count += 1
      end
    end
    @search = @search.search(params[:search])
    if new.length > 0
      params[:search] = params[:search].merge(new)
    end


    @workingwells = @search.paginate(:page => params[:page])
    # @ww_csv = @search.all

    respond_to do |format|
      format.html
      # format.csv { send_data @ww_csv.to_csv }
    end

  end

  # GET /workingwells/1
  # GET /workingwells/1.json
  def show
    @workingwell = Workingwell.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @workingwell }
    end
  end

  # GET /workingwells/new
  # GET /workingwells/new.json
  def new
    @workingwell = Workingwell.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workingwell }
    end
  end

  # GET /workingwells/1/edit
  def edit
    @workingwell = Workingwell.find(params[:id])
  end

  # POST /workingwells
  # POST /workingwells.json
  def create
    @workingwell = Workingwell.new(params[:workingwell])
    @workingwell.update_attributes(:user_id => current_user.id)

    respond_to do |format|
      if @workingwell.save
        format.html { redirect_to @workingwell, notice: 'Workingwell was successfully created.' }
        format.json { render json: @workingwell, status: :created, location: @workingwell }
      else
        format.html { render action: "new" }
        format.json { render json: @workingwell.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /workingwells/1
  # PUT /workingwells/1.json
  def update
    @workingwell = Workingwell.find(params[:id])

    respond_to do |format|
      if current_user.id == @workingwell.user_id && @workingwell.is_control?
        if @workingwell.update_attributes(params[:workingwell])
          format.html { redirect_to workingplate_path(@workingwell.workingplate.id), notice: 'Workingwell was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @workingwell.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to workingplate_path(@workingwell.workingplate.id), notice: 'Workingwell record does not belong to you or is no control.' }
      end

    end
  end


  def add_to_compare
    @workingwell = Workingwell.find(params[:id])
    session[:compare] = {} unless session[:compare].present?
    session[:compare].merge!(:"#{params[:id]}" => params[:id])
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
    
  end

  def remove_from_compare
    @workingwell = Workingwell.find(params[:id])
    session[:compare] = {} unless session[:compare].present?
    session[:compare].delete(:"#{params[:id]}")
    respond_to do |format|
      format.html { redirect_to :back}
      format.js
    end

    
  end

  def clear_comparison_cart
    @workingwell_ids = format_session_compare(session[:compare])
    session[:compare] = {}
    respond_to do |format|
      format.html { redirect_to :back, notice: "The Comparisons have been cleared." }
      format.js
    end

  end

  protected

  def format_session_compare(session)
    string = []
    session.each do |k,id|
      string << "{ workingwell_id: #{id}}"
    end
    return string.join(",")
  end

end
