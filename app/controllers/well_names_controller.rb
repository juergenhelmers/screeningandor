###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class WellNamesController < ApplicationController
  def index
    @names = Workingwell.order(:name).where("name ilike ?", "%#{params[:term]}%")
    render json: @names.map(&:name)
  end
end
