###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class MetadataRecordValuesController < ApplicationController
  def index
    @records = MetadataRecord.order(:value).where(metadatarecordable_type: "Workingwell").where("value ilike ?", "%#{params[:term]}%")
    render :json => @records.map(&:value)
  end
end
