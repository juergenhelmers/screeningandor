# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->

  $("table tr td a.popover_link, table tr td span.popover_link").popover()

  $("#search_workingplate_label_contains").autocomplete
    source: $("#search_workingplate_label_contains").data("autocomplete-source")
    minLength: 2
  $("#search_name_contains").autocomplete
    source: $("#search_name_contains").data("autocomplete-source")
    minLength: 2
  $("#search_uuid_contains").autocomplete
    source: $("#search_uuid_contains").data("autocomplete-source")
    minLength: 2
  $("#search_catalog_number_contains").autocomplete
    source: $("#search_catalog_number_contains").data("autocomplete-source")
    minLength: 2
  $("#search_precursor_accession_contains").autocomplete
    source: $("#search_precursor_accession_contains").data("autocomplete-source")
    minLength: 2
  $("#search_mature_sanger_id_contains").autocomplete
    source: $("#search_mature_sanger_id_contains").data("autocomplete-source")
    minLength: 2
  $("#search_mature_sequence_contains").autocomplete
    source: $("#search_mature_sequence_contains").data("autocomplete-source")
    minLength: 2
  $("#search_metadata_records_value_contains").autocomplete
    source: $("#search_metadata_records_value_contains").data("autocomplete-source")
    minLength: 2

  # compare cart logic
  count = $("#user_nav.raised.well_comparison table tr").length
  if count <= 1
    $("#user_nav.raised.well_comparison table").hide()
    $('#compare_actions_links').hide()
    $("#empty_cart").show()
  else
    $("#user_nav.raised.well_comparison table").show()
    $('#compare_actions_links').show()
    $("#empty_cart").hide()

  # grouped_collection_selection on screen drop down logic
  screens = $('#search_workingplate_masterplate_screen_name_contains').html()
  $('#search_workingplate_masterplate_screen_user_username_contains').change ->
    user = $('#search_workingplate_masterplate_screen_user_username_contains :selected').text()
    escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(screens).filter("optgroup[label='#{escaped_user}']").html()
    if options
      $('#search_workingplate_masterplate_screen_name_contains').html(options)
    else if user == "Select a user"
      $('#search_workingplate_masterplate_screen_name_contains').html(screen)  
    else
      $('#search_workingplate_masterplate_screen_name_contains').empty()
