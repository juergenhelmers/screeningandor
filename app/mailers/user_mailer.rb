###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

class UserMailer < ActionMailer::Base
  default from: "no-reply@lorax.synbiotic.csir.co.za",
          replyto: "no-reply@lorax.synbiotic.csir.co.za"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.deliver_welcome_email.subject
  #
  def deliver_welcome_email(user)
    @user = user
    mail to: user.email, subject: "Sign-up Confirmation Notice"
  end

  def deliver_signup_notification_email(admin,user)
    @user = user
    if admin.length == 0
      recepients = "juergen.helmers@gmail.com"
    else
      recepients = admin.collect{|a| a.email}.join(",")
    end

    mail to: recepients, subject: "[Mhlangalab:Screening] Sign-up Notification"
  end

  def account_approval(user)
    @user = user
    mail to: user.email, subject: "Your Account Has Been Approved!"
  end
end
