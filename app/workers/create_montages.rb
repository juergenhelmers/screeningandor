###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'uuidtools'
require 'fileutils'
require 'sequel'
require 'escape'
require 'time'
require 'yaml'

class CreateMontages

  def self.update_workingplate(workingplate_id,db_seq)
    db_seq[:workingplates].filter(:id => workingplate_id).update(:processing => false, :imported => true)
  end

  def self.perform(dir,image_files,workingplate_id,db_seq,number_channels,channel_names,app_config)
    FileUtils.mkdir_p "#{dir}/composites"                         # create new subdirectory in case it does not exist
    image_pairs = image_files.each_slice(number_channels).to_a    # sort into 2D array group per composite image
    image_pairs.each do |image|                                   # create a composite image combining the two channels
      if number_channels == 2
        dapi_image = image[channel_names.index("DAPI").to_i]
        fitc_image = image[channel_names.index("FITC").to_i]
        composite_images = %x[convert -geometry 150x150 '#{fitc_image}' '#{dapi_image}' -background black -channel green,blue -combine '#{dir}/composites/#{File.basename(dapi_image).gsub("_w1","").gsub(".TIF",".png")}']
      end
    end
    # create a montage per well
    FileUtils.mkdir_p "#{dir}/montages"                            # create new subdirectory in case it does not exist
    images = Dir[dir + "/composites/**/*.png"].sort                # read in all composite images
    images_per_well = images.each_slice(4).to_a                    # group per well
    images_per_well.each do |images|
      montage_images = %x[montage -geometry +0+0 -tile 2x2 '#{images[0]}' '#{images[1]}' '#{images[2]}' '#{images[3]}' '#{dir}/montages/#{File.basename(images[0]).gsub("_w1","").gsub("_s1","").gsub(" ", "_")}']
    end

    # save the montage with the workingwell
    montages = Dir[dir + "/montages/**/*.png"].sort                                           # read in all composite images
    workingplate = db_seq[:workingplates].filter(:id => workingplate_id).first
    #workingwells = workingplate.workingwells.where("name != ? AND name != ?", "PBS", "empty") # get all active workingwells
    workingwells = db_seq[:workingwells].filter(:workingplate_id => workingplate[:id]).exclude(:name => "PBS").exclude(:name => "empty")

    workingwells.each_with_index do |well,index|
      # remove the remote_file_root_directory from path
      montage_url = montages[index].gsub("#{app_config['remote_file_root_directory']}","")
      #db_seq[:workingwells].filter(:id => well[:id]).update(:montage_url => "#{montages[index]}")
      # save the montage_url with the well record
      db_seq[:workingwells].filter(:id => well[:id]).update(:montage_url => montage_url)
    end

    update_workingplate(workingplate_id,db_seq)
  end
end
