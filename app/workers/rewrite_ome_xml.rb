###
# Copyright (C) 2012 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

require 'rexml/document'
include REXML
require 'uuidtools'
require 'fileutils'
require 'sequel'
require 'escape'
require 'time'
require 'yaml'

module RewriteOmeXml

  @queue = :ome_rewrite # define the queue the worker is assigned to
  #@queue = :debug # define the queue the worker is assigned to

  def self.write_channel_string(dir,channel_names)
    raw_htd_file = File.open(Dir[dir + "/**/*.{htd,HTD}"][0])                                         # get the full path to HTD file
    htd_file_base_name = File.basename(raw_htd_file).gsub(".htd", "").gsub("HTD", "").gsub(" ", "_")  # get the basename of htd file
    channel_name_string = channel_names.join(",")                                                     # convert array into ,-sep string

    channelFile = File.new("#{dir}/#{htd_file_base_name}channel", "w")                                # create a new .channel file in same dir as HTD file
    channelFile.write(channel_name_string)                                                            # write the channel name string into the new file
    channelFile.close                                                                                 # close the file
  end



  def self.create_composite_images(dir,image_files,workingplate_id,db_seq,number_channels,channel_names,app_config)
    FileUtils.mkdir_p "#{dir}/composites"                         # create new subdirectory in case it does not exist
    image_pairs = image_files.each_slice(number_channels).to_a    # sort into 2D array group per composite image
    image_pairs.each do |image|                                   # create a composite image combining the two channels
      if number_channels == 2
        dapi_image = image[channel_names.index("DAPI").to_i]
        fitc_image = image[channel_names.index("FITC").to_i]
        composite_images = %x[convert -geometry 150x150 '#{fitc_image}' '#{dapi_image}' -background black -channel green,blue -combine '#{dir}/composites/#{File.basename(dapi_image).gsub("_w1","").gsub(".TIF",".png")}']
      end
    end
    # create a montage per well
    FileUtils.mkdir_p "#{dir}/montages"                            # create new subdirectory in case it does not exist
    images = Dir[dir + "/composites/**/*.png"].sort                # read in all composite images
    images_per_well = images.each_slice(4).to_a                    # group per well
    images_per_well.each do |images|
      montage_images = %x[montage -geometry +0+0 -tile 2x2 '#{images[0]}' '#{images[1]}' '#{images[2]}' '#{images[3]}' '#{dir}/montages/#{File.basename(images[0]).gsub("_w1","").gsub("_s1","").gsub(" ", "_")}']
    end

    # save the montage with the workingwell
    montages = Dir[dir + "/montages/**/*.png"].sort                                           # read in all composite images
    workingplate = db_seq[:workingplates].filter(:id => workingplate_id).first
    #workingwells = workingplate.workingwells.where("name != ? AND name != ?", "PBS", "empty") # get all active workingwells
    workingwells = db_seq[:workingwells].filter(:workingplate_id => workingplate[:id]).exclude(:name => "PBS").exclude(:name => "empty")

    workingwells.each_with_index do |well,index|
      # remove the remote_file_root_directory from path
      montage_url = montages[index].gsub("#{app_config['remote_file_root_directory']}","")
      #db_seq[:workingwells].filter(:id => well[:id]).update(:montage_url => "#{montages[index]}")
      # save the montage_url with the well record
      db_seq[:workingwells].filter(:id => well[:id]).update(:montage_url => montage_url)
    end

  end

  def self.create_metamorph_ome_xml_header(image,entry,active_workingwell,row_counter,column_counter,image_counter,channel_counter,db_seq,number_channels,channel_names)

        workingplate = db_seq[:workingplates].filter(:id => active_workingwell[:workingplate_id]).first  # get the workingplate from active_well

        header = entry['ImageDescription']                    # get the header from entry hash
        date = entry['DateTime'].split(" ")[0].gsub(":", "-") # get the date from entry hash
        time = entry['DateTime'].split(" ")[1]                # get the time from entry hash
        time_stamp = date + "T" + time                        # create OME timestamp
        with_eq = header.scan(/^.*=.*/)                       # extract needed header information
        with_eq.each do |line|                                # extract all tag lines that are split by "="
          tag, value = line.split("=")
          tag.strip!; value.strip!
          entry[tag] = value
        end
        entry["Laser"] = header.scan(/^.*nm laser.*/)         # extract all tag lines with *nm laser*
        entry["Laser"].each do |val|
          val.chomp!
        end
        entry["Py"] = header.scan(/^.*1 to.*/)                # extract all tag lines with *1 to*
        entry["Py"].each do |val|
          val.chomp!
        end
        entry["Gain"] = header.scan(/^.*at.*gain.*/)          # extract all tag lines with *at*gain*
        entry["Gain"].each do |val|
          val.chomp!
        end
        # empty xml header structure
        empty_xml = %{<?xml version="1.0" encoding="UTF-8"?><OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2011-06" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ROI="http://www.openmicroscopy.org/Schemas/ROI/2011-06" xmlns:SA="http://www.openmicroscopy.org/Schemas/SA/2011-06"  xmlns:SPW="http://www.openmicroscopy.org/Schemas/SPW/2011-06" xmlns:Bin="http://www.openmicroscopy.org/Schemas/BinaryFile/2011-06"  xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2011-06 http://www.openmicroscopy.org/Schemas/OME/2011-06/ome.xsd"></OME>}
        # generate new OME-XML structure based on empty xml string
        doc = REXML::Document.new empty_xml
        image_uuid = UUIDTools::UUID.timestamp_create().to_s
        uuid = doc.elements['OME'].add_attribute("uuid", "urn:uuid:#{image_uuid}")
        plate = doc.elements['OME'].add_element('SPW:Plate', {"ID" => "Plate:0", "Name" => "#{workingplate[:label]}", "ColumnNamingConvention" => "number", "RowNamingConvention" => "letter", "Columns" => 12, "Rows" => 8})
        plate_description = doc.elements["//SPW:Plate"].add_element("SPW:Description", "#{workingplate[:description]}")
        screen_ref = doc.elements['//SPW:Plate'].add_element("SPW:ScreenRef", {"ID" => "Screen:1"})
        well = doc.elements['//SPW:Plate'].add_element("SPW:Well", {"ID" => "Well:1.1.1", "Column" => "#{column_counter + 1}", "Row" => "#{row_counter + 1}", "ExternalIdentifier" => "#{active_workingwell[:uuid]}"})
        reagent_ref = doc.elements['//SPW:Well'].add_element("ReagentRef", {"ID" => "Reagent:0"})
        well_sample = doc.elements['//SPW:Well'].add_element("SPW:WellSample", {"ID" => "WellSample:1.1.1.#{image_counter}", "Index" => "#{image_counter}", "TimePoint" => ""})
        image_ref = doc.elements['//SPW:WellSample'].add_element("SPW:ImageRef", {"ID" => "Image:0"})
        plate_acquisition = doc.elements['//SPW:Plate'].add_element("SPW:PlateAcquisition", {"xmlns" => "http://www.openmicroscopy.org/Schemas/SPW/2010-06", "ID" => "PlateAcquisition:Plate:1:ScreenAcquisition:1", "StartTime" => "2010-02-23T12:49:30", "EndTime" => "2010-02-23T12:50:30"})
        screen = doc.elements['OME'].add_element("SPW:Screen", {"ID" => "Screen:1", "ProtocolDescription" => "", "ProtocolIdentifier" => "", "ReagentSetDescription" => "", "ReagentSetIdentifier" => "", "Name" => "#{db_seq[:screens].filter(:id => db_seq[:masterplates].filter(:id => workingplate[:masterplate_id]).first[:screen_id]).first[:name]}", "Type" => ""})
        reagent = doc.elements['OME'].add_element("Reagent", {"ID" => "Reagent:0"})
        reagent_name = reagent.add_element("Name")
        reagent_name_value = reagent_name.add_text("#{active_workingwell[:name]}")
        reagent_description = reagent.add_element("Description", {"description" => ""})
        reagent_identifier = reagent.add_element("ReagentIdentifier", {"uuid" => "#{active_workingwell[:uuid]}"})

        # get a list of metadata_records attached to active_workingwell
        metadata_records = db_seq[:metadata_records].filter(:metadatarecordable_type => "Workingwell").filter(:metadatarecordable_id => "#{active_workingwell[:id]}").all
        if metadata_records.length > 0
          metadata_records.each_with_index do |metadatarecord,index|
            reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{index}:#{metadatarecord[:name].gsub(' ','_')}"})
            @index_counter = index
          end
        else
          @index_counter = -1
        end

        reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+1}:IsControl"})
        if active_workingwell[:is_control]
          reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+2}:Substance"})
          reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+3}:Concentration"})
          reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+4}:Buffer"})
        end
        structured_annotations = doc.elements['OME'].add_element("StructuredAnnotations")
        # loop over all metadata_records and add structured annotation nodes if present
        if metadata_records.length > 0
          metadata_records.each_with_index do |metadatarecord,index|
            text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{index}:#{metadatarecord[:name].gsub(' ','_')}"})
            value0 = text_annotation0.add_element("Value").add_text("#{metadatarecord[:name].gsub(" ","_").downcase}:#{metadatarecord[:value]}")
            @index_counter = index
          end
        else
          @index_counter = -1
        end

        text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+1}:IsControl"})
        value0 = text_annotation0.add_element("Value").add_text("is_control:#{active_workingwell[:is_control]}")
        if active_workingwell[:is_control]
          text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+2}:Substance"})
          value0 = text_annotation0.add_element("Value").add_text("substance:#{active_workingwell[:substance]}")
          text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+3}:Concentration"})
          value0 = text_annotation0.add_element("Value").add_text("concentration:#{active_workingwell[:concentration]}")
          text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+4}:Buffer"})
          value0 = text_annotation0.add_element("Value").add_text("buffer:#{active_workingwell[:buffer]}")
        end

        image_xml = doc.elements['OME'].add_element("Image", {"ID" => "Image:0", "Name" => "#{File.basename(image)}"})
        image_acquired_date = image_xml.add_element("AcquiredDate").add_text("#{time_stamp}")
        pixel_uuid = UUIDTools::UUID.timestamp_create().to_s
        pixels = image_xml.add_element("Pixels", {"DimensionOrder" => "XYZCT", "ID" => "urn:lsid:synbio.csir.co.za:Pixels:#{pixel_uuid}:13570", "PhysicalSizeX" => "0.4", "PhysicalSizeY" => "0.4", "PhysicalSizeZ" => "1.0", "SizeC" => "2", "SizeT" => "1", "SizeX" => "#{entry['ImageWidth']}", "SizeY" => "#{entry['ImageLength']}", "SizeZ" => "1", "TimeIncrement" => "1.0", "Type" => "uint16"})
        if channel_counter == 1
          channel0 = pixels.add_element("Channel", {"Color" => "-16776961", "ID" => "urn:lsid:synbio.csir.co.za:Channel:#{pixel_uuid}:13572","SamplesPerPixel" => "1", "PinholeSize" => "#{entry['Pinhole diameter']}"})
        else
          channel0 = pixels.add_element("Channel", {"Color" => "16711935", "ID" => "urn:lsid:synbio.csir.co.za:Channel:#{pixel_uuid}:13572","SamplesPerPixel" => "1", "PinholeSize" => "#{entry['Pinhole diameter']}"})
        end
        lightpath = channel0.add_element("LightPath")
        tiff_data0 = pixels.add_element("TiffData", {"FirstC" => "0", "FirstT" => "0", "FirstZ" => "0", "IFD" => "0", "PlaneCount" => "1"})
        uuid_filename = tiff_data0.add_element("UUID", {"FileName" => "#{File.basename(image)}"}).add_text("#{image_uuid}")
        plane0 = pixels.add_element("Plane", {"DeltaT" => "0.592", "PositionX" => "54900.0", "PositionY" => "22000.0", "PositionZ" => "49.99", "TheC" => "0", "TheT" => "0", "TheZ" => "0"})
        # convert rexml object to a string
        doc_xml = doc.to_s
        return doc_xml
      end

  def self.read_image_directory number_channels
    count = 1
    ultra_images = []
    while count <= number_channels do
      ultra_images << Dir[dir + "/**/*#{count}.TIF"]
    end
    ultra_images.flatten.sort
  end

  def self.metamorph_import(dir,workingplate_id,db_seq,app_config,number_channels,channel_names)
    write_channel_string(dir,channel_names)

    image_files = read_image_directory number_channels

    channels_per_image = image_files.each_slice(number_channels).to_a  # group the separated channels per image each into 2D array
    images_per_well = channels_per_image.each_slice(4).to_a # with 4 images per well group them per well

    workingplate = db_seq[:workingplates].filter(:id => workingplate_id).first
    workingwells_seq = db_seq[:workingwells].filter(:workingplate_id => workingplate_id).exclude(:name => "PBS")  # exclude all PBS wells
    workingwells = []                 # initialize an empty array to hold the hashes
    workingwells_seq.each do |well|   # loop over the Sequel object
      workingwells << well            # add each record hash to the array
    end
    workingwells_in_rows_and_columns = workingwells.each_slice(10).to_a # sort the wells into rows of 10 wells each according to plate layout

    row_counter = 0  # initialize counter for image and annotation array
    well_counter = 0 # have a separated counter for the image array

    while row_counter < workingwells_in_rows_and_columns.length  # loop over number of rows
      column_counter = 0
      while column_counter < workingwells_in_rows_and_columns[row_counter].length
        unless workingwells_in_rows_and_columns[row_counter][column_counter][:name] == "empty"
          image_counter = 0
          active_workingwell = workingwells_in_rows_and_columns[row_counter][column_counter]
          while image_counter < 4
            channel_counter = 0
            while channel_counter < number_channels
              raw = %x["#{app_config['exiv2']}" -pa "#{images_per_well[well_counter][image_counter][channel_counter]}"]
              # extract a ruby hash from the raw exiv2 header output
              # intialize entry as an empty hash
              entry = {}
              # split string data into single exif tags
              tag_lines = raw.split("Exif.Image.")

              tag_lines.each do |line|
                line.chop! # chop off \n
                line.gsub!(/ {2,}/, "\t")   # replace more than one space into a single tab for consistent splitting
                name, datatype, count, data = line.split("\t") # split at \n for different data subsets
                entry[name] = data          # add tag data to result hash
              end
              # function call to generate the OME-XML structure
              ome_xml = create_metamorph_ome_xml_header(images_per_well[well_counter][image_counter][channel_counter],entry, active_workingwell,row_counter, column_counter,image_counter,channel_counter,db_seq,number_channels,channel_names)
              # TODO delete existing OME XML structure if exists!
              new_header = (ome_xml + entry['ImageDescription']).to_s   # combine OME-XML and old header
              # write the combined header to file
              command = Escape.shell_command(["#{app_config['tiffcomment']}", '-set', "#{new_header}", "#{images_per_well[well_counter][image_counter][channel_counter]}"])
              write_header = %x[#{command}]
              channel_counter += 1
            end
            image_counter += 1
          end
          well_counter += 1
          column_counter += 1
        else
          column_counter += 1
        end
      end
      row_counter += 1
    end
    Resque.enqueue(CreateMontages, dir,image_files,workingplate_id,db_seq,number_channels,channel_names,app_config)
  end

## ANDOR CODE
  def self.create_andor_ome_xml_header(image,workingplate_id,row_counter,column_counter,image_counter,active_workingwell,time_stamp,image_height,image_width,db_seq)
    workingplate = db_seq[:workingplates].filter(:id => workingplate_id).first  # get the workingplate
    # generate a new header for each file
    # use initial XML string to create XML object
    empty_xml = %{<?xml version="1.0" encoding="UTF-8"?><OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2011-06" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ROI="http://www.openmicroscopy.org/Schemas/ROI/2011-06" xmlns:SA="http://www.openmicroscopy.org/Schemas/SA/2011-06"  xmlns:SPW="http://www.openmicroscopy.org/Schemas/SPW/2011-06" xmlns:Bin="http://www.openmicroscopy.org/Schemas/BinaryFile/2011-06"  xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2011-06 http://www.openmicroscopy.org/Schemas/OME/2011-06/ome.xsd"></OME>}
    doc = REXML::Document.new empty_xml                                         # create XML object
    image_uuid = UUIDTools::UUID.timestamp_create().to_s                        # add image file UUID to OME block
    uuid = doc.elements['OME'].add_attribute("uuid", "urn:uuid:#{image_uuid}")
    plate = doc.elements['OME'].add_element('SPW:Plate', {"ID" => "Plate:0", "Name" => "#{workingplate[:label]}", "ColumnNamingConvention" => "number", "RowNamingConvention" => "letter", "Columns" => 12, "Rows" => 8})
    plate_description = doc.elements["//SPW:Plate"].add_element("SPW:Description", "#{workingplate[:description]}")
    screen_ref = doc.elements['//SPW:Plate'].add_element("SPW:ScreenRef", {"ID" => "Screen:1"})
    well = doc.elements['//SPW:Plate'].add_element("SPW:Well", {"ID" => "Well:1.1.1", "Column" => "#{column_counter}", "Row" => "#{row_counter}", "ExternalIdentifier" => "#{active_workingwell[:uuid]}"})
    reagent_ref = doc.elements['//SPW:Well'].add_element("ReagentRef", {"ID" => "Reagent:0"})
    well_sample = doc.elements['//SPW:Well'].add_element("SPW:WellSample", {"ID" => "WellSample:1.1.1.#{image_counter}", "Index" => "#{image_counter}", "TimePoint" => ""})
    image_ref = doc.elements['//SPW:WellSample'].add_element("SPW:ImageRef", {"ID" => "Image:0"})
    plate_acquisition = doc.elements['//SPW:Plate'].add_element("SPW:PlateAcquisition", {"xmlns" => "http://www.openmicroscopy.org/Schemas/SPW/2010-06", "ID" => "PlateAcquisition:Plate:1:ScreenAcquisition:1", "StartTime" => "2010-02-23T12:49:30", "EndTime" => "2010-02-23T12:50:30"})
    screen = doc.elements['OME'].add_element("SPW:Screen", {"ID" => "Screen:1", "ProtocolDescription" => "", "ProtocolIdentifier" => "", "ReagentSetDescription" => "", "ReagentSetIdentifier" => "", "Name" => "#{db_seq[:screens].filter(:id => db_seq[:masterplates].filter(:id => workingplate[:masterplate_id]).first[:screen_id]).first[:name]}", "Type" => ""})
    reagent = doc.elements['OME'].add_element("Reagent", {"ID" => "Reagent:0"})
    reagent_name = reagent.add_element("Name")
    reagent_name_value = reagent_name.add_text("#{active_workingwell[:name]}")
    reagent_description = reagent.add_element("Description", {"description" => ""})
    reagent_identifier = reagent.add_element("ReagentIdentifier", {"uuid" => "#{active_workingwell[:uuid]}"})
    # get a list of metadata_records attached to active_workingwell
    metadata_records = db_seq[:metadata_records].filter(:metadatarecordable_type => "Workingwell").filter(:metadatarecordable_id => "#{active_workingwell[:id]}").all
    if metadata_records.length > 0
      metadata_records.each_with_index do |metadatarecord,index|
        reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{index}:#{metadatarecord[:name].gsub(' ','_')}"})
        @index_counter = index
      end
    else
      @index_counter = -1
    end

    reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+1}:IsControl"})
    if active_workingwell[:is_control]
      reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+2}:Substance"})
      reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+3}:Concentration"})
      reagent_annotation0 = reagent.add_element("AnnotationRef", {"ID" => "Annotation:#{@index_counter+4}:Buffer"})
    end
    structured_annotations = doc.elements['OME'].add_element("StructuredAnnotations")
    # loop over all metadata_records and add structured annotation nodes if present
    if metadata_records.length > 0
      metadata_records.each_with_index do |metadatarecord,index|
        text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{index}:#{metadatarecord[:name].gsub(' ','_')}"})
        value0 = text_annotation0.add_element("Value").add_text("#{metadatarecord[:name].gsub(" ","_").downcase}:#{metadatarecord[:value]}")
        @index_counter = index
      end
    else
      @index_counter = -1
    end

    text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+1}:IsControl"})
    value0 = text_annotation0.add_element("Value").add_text("is_control:#{active_workingwell[:is_control]}")
    if active_workingwell[:is_control]
      text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+2}:Substance"})
      value0 = text_annotation0.add_element("Value").add_text("substance:#{active_workingwell[:substance]}")
      text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+3}:Concentration"})
      value0 = text_annotation0.add_element("Value").add_text("concentration:#{active_workingwell[:concentration]}")
      text_annotation0 = structured_annotations.add_element("TextAnnotation", {"ID" => "Annotation:#{@index_counter+4}:Buffer"})
      value0 = text_annotation0.add_element("Value").add_text("buffer:#{active_workingwell[:buffer]}")
    end
    image_xml = doc.elements['OME'].add_element("Image", {"ID" => "Image:0", "Name" => "#{File.basename(image)}"})
    image_acquired_date = image_xml.add_element("AcquiredDate").add_text("#{time_stamp.strftime("%Y-%m-%dT%H:%M:%S")}")
    pixel_uuid = UUIDTools::UUID.timestamp_create().to_s
    pixels = image_xml.add_element("Pixels", {"DimensionOrder" => "XYZCT", "ID" => "urn:lsid:synbio.csir.co.za:Pixels:#{pixel_uuid}:13570", "PhysicalSizeX" => "0.4", "PhysicalSizeY" => "0.4", "PhysicalSizeZ" => "1.0", "SizeC" => "2", "SizeT" => "1", "SizeX" => "#{image_width}", "SizeY" => "#{image_height}", "SizeZ" => "1", "TimeIncrement" => "1.0", "Type" => "uint16"})
    channel0 = pixels.add_element("Channel", {"Color" => "-16776961", "ID" => "urn:lsid:synbio.csir.co.za:Channel:#{pixel_uuid}:13572", "SamplesPerPixel" => "1"})
    lightpath = channel0.add_element("LightPath")
    channel1 = pixels.add_element("Channel", {"Color" => "16711935", "ID" => "urn:lsid:synbio.csir.co.za:Channel:#{pixel_uuid}:13572", "SamplesPerPixel" => "1"})
    lightpath = channel0.add_element("LightPath")
    tiff_data0 = pixels.add_element("TiffData", {"FirstC" => "0", "FirstT" => "0", "FirstZ" => "0", "IFD" => "0", "PlaneCount" => "1"})
    uuid_filename = tiff_data0.add_element("UUID", {"FileName" => "#{File.basename(image)}"}).add_text("#{image_uuid}")
    tiff_data1 = pixels.add_element("TiffData", {"FirstC" => "1", "FirstT" => "0", "FirstZ" => "0", "IFD" => "1", "PlaneCount" => "1"})
    uuid_filename = tiff_data1.add_element("UUID", {"FileName" => "#{File.basename(image)}"}).add_text("#{image_uuid}")
    plane0 = pixels.add_element("Plane", {"DeltaT" => "0.592", "PositionX" => "54900.0", "PositionY" => "22000.0", "PositionZ" => "49.99", "TheC" => "1", "TheT" => "0", "TheZ" => "0"})
    plane1 = pixels.add_element("Plane", {"DeltaT" => "0", "PositionX" => "54900.0", "PositionY" => "22000.0", "PositionZ" => "49.99", "TheC" => "0", "TheT" => "0", "TheZ" => "0"})
    doc_xml = doc.to_s                        # convert rexml object to string
    return doc_xml                            # return the XML string
  end

  def self.andor_import(dir,workingplate_id,db_seq,app_config)

    image_files = Dir[dir + "/**/*.tif"].sort # get sorted list of images in the source directory
    image_files_per_well = image_files.each_slice(9).to_a # group 9 images each into 2D array
    images_per_well_in_rows_and_columns = image_files_per_well.each_slice(10).to_a  # group into 6 rows of 10 columns each

    workingplate = db_seq[:workingplates].filter(:id => workingplate_id).first
    workingwells_seq = db_seq[:workingwells].filter(:workingplate_id => workingplate_id).exclude(:name => "PBS")  # exclude all PBS wells
    workingwells = []                 # initialize an empty arry to hold the hashes
    workingwells_seq.each do |well|   # loop over the Sequel object
      workingwells << well            # add each record hash to the array
    end


    if workingwells.length == 60
      workingwells_in_rows = workingwells.each_slice(10).to_a
      workingwells_in_snake_order = []                        # initalize output array
      i = 0                                                   # intialize counter
      while i < workingwells_in_rows.length                   # loop over 2D array and reverse each odd row to acount for reverse
        if i.even?                                            # scanning order (right -> left)
          workingwells_in_snake_order << workingwells_in_rows[i]
        else
          workingwells_in_snake_order << workingwells_in_rows[i].reverse
        end
        i += 1
      end
    end

    row_counter = 0                         # initialize counter for image and annotation array
    while row_counter < 6
      column_counter = 0
      while column_counter < 10
        image_counter = 0
        while image_counter < 9
          active_workingwell = workingwells_in_snake_order[row_counter][column_counter]
          image = images_per_well_in_rows_and_columns[row_counter][column_counter][image_counter]

          # get original header from image file
          header_description_raw = %x["#{app_config['exiv2']}" -pa -g Exif.Image.ImageDescription "#{image}"]
          # remove the first line which contains the Exif-key
          header_description = header_description_raw.lines.to_a[1..-1].join
          # add the first value of the original hash value back to beginning of string
          header_description = "[Environment]" + "\r\n" + header_description
          # extract needed header information
          raw_date_string = header_description.scan(/Date=.*/)[0].to_s.strip
          key, date = raw_date_string.split("=")
          raw_time_string = header_description.scan(/Time=*.........../)[0].to_s.strip
          key, time = raw_time_string.split("=")
          time_new = time.gsub(" PM", "").gsub(" AM", "").split(":")
          if time.include? "AM"
            time_new[0] =+13
          end
          date_new = date.split("/")
          time_stamp = Time.local(date_new[2],date_new[1],date_new[0],time_new[0],time_new[1],time_new[2])
          raw_size_x = header_description.scan(/^*Image Width=.*/)[0].strip
          raw_size_y = header_description.scan(/^*Image Height=.*/)[0].strip
          key, image_width = raw_size_x.split("=")
          key, image_height = raw_size_y.split("=")
          ome_header = create_andor_ome_xml_header(image,workingplate_id,row_counter,column_counter,image_counter,active_workingwell,time_stamp,image_height,image_width,db_seq)
          new_header = ome_header + header_description # combine old and new header
          # write new and combined header to each file
          write_new_header = %x["#{app_config['tiffcomment']}" -set "#{new_header}" "#{image}"]
          image_counter += 1
        end
        column_counter += 1
      end
      row_counter += 1
    end
    update_workingplate(workingplate_id,db_seq) # update imported/processing flags of workingplate
  end




  def self.perform(options)                             # default resque worker functions that gets called on enqueue
    dir = options['dir']                                # get dir variable out of the options hash
    workingplate_id = options['workingplate_id']        # get working_id variable out of the options hash
    image_type = options['image_type']                  # get image_type variable out of the options hash
    env = options['rails_env']                          # get the current rails environment of the controller
    number_channels = options['number_channels'].to_i   # get the number of channels from the options hash
    channel_names = []                                  # create an empty arry for the channel names
    options['channel_names'].each do |k,v|              # get the the hash containing the channel names from the options hash
      channel_names << v                                # save the actual channel names in an array
    end

    database   = YAML::load( File.open( 'config/database.yml' ) )["#{env}"]   # read in the database config file
    app_config = YAML::load( File.open( 'config/config.yml') )["#{env}"]      # read in the application config file
    db_seq = Sequel.connect("#{database['adapter'].gsub("postgresql", "postgres")}://#{database['username']}:#{database['password']}@#{database['host']}:#{database['port']}/#{database['database']}")

    if image_type == "Andor"                                                                  # check if the images_type is Andor
      andor_import(dir,workingplate_id,db_seq,app_config)                                     # call the andor importing function
    elsif image_type == "MetaMorph"                                                           # check if the images_type is MetaMorph
      metamorph_import(dir,workingplate_id,db_seq,app_config,number_channels,channel_names)   # call the metamorph importing function
    end
  end

end
